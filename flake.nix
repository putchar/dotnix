{
  description = "Flake for putchar";

  inputs = {
    ## https://github.com/NixOS/nix
    ##nix.url = "github:NixOS/nix";
    ## https://github.com/NixOS/nixpkgs/tree/nixos-22.05
    nixos-oldStable.url = "github:NixOS/nixpkgs/nixos-22.11";
    ## https://github.com/NixOS/nixpkgs/tree/nixos-22.11
    nixos-stable.url = "github:NixOS/nixpkgs/nixos-23.05";
    ## https://github.com/NixOS/nixpkgs/tree/nixos-unstable
    nixos-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
    ## https://github.com/NixOS/nixpkgs
    nixpkgs-master.url = "github:NixOS/nixpkgs/master";

    ## https://github.com/NixOS/nixos-hardware
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";

    ## https://github.com/nix-community/home-manager
    home-manager.url = "github:nix-community/home-manager/master";
    home-manager.inputs.nixpkgs.follows = "nixos-unstable";

    ## https://github.com/serokell/deploy-rs
    deploy-rs.url = "github:serokell/deploy-rs";
    deploy-rs.inputs.nixpkgs.follows = "nixos-unstable";

    ## Extra flake hosting my blog
    blog.url = "gitlab:putchar.net/blog";
    blog.inputs.nixpkgs.follows = "nixos-unstable";

    ## https://github.com/tfutils/tfenv
    tfenv.url = "github:tfutils/tfenv";
    tfenv.flake = false;

    #    ## https://github.com/nix-community/nixos-generators
    #    nixos-generators = {
    #      url = "github:nix-community/nixos-generators";
    #      inputs.nixpkgs.follows = "nixos-unstable";
    #    };

    #lix = {
    #  url = "git+https://git@git.lix.systems/lix-project/lix?ref=refs/tags/2.90-beta.1";
    #  flake = false;
    #};
    lix-module = {
      url = "https://git.lix.systems/lix-project/nixos-module/archive/2.90.0.tar.gz";
      #inputs.lix.follows = "lix";
      inputs.nixpkgs.follows = "nixos-unstable";
    };
  };

  outputs = inputs @ {self, ...}: let
    myLib = import ./lib inputs;
    lib = inputs.nixos-unstable.lib;

    supportedSystems = [
      "aarch64-linux"
      "x86_64-linux"
    ];
    forAllSystems = lib.genAttrs supportedSystems;
  in {
    devShells = forAllSystems (system: {
      default = inputs.nixos-unstable.legacyPackages.${system}.mkShell {
        name = "putchar-dotnix-on-${system}-system";
        packages = builtins.attrValues {
          # when in doubt and you wish to remove some commits
          ## https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/removing-sensitive-data-from-a-repository
          #bfg-repo-cleaner

          inherit
            (inputs.nixos-unstable.legacyPackages.${system})
            gnumake
            nvd
            scc
            vim
            #nix
            
            git
            nixos-rebuild
            alejandra
            colmena
            ;
          inherit (inputs.home-manager.packages.${system}) home-manager;
          inherit (inputs.deploy-rs.packages.${system}) deploy-rs;
        };
      };
    });

    ## cf https://gitlab.com/putchar/dotnix/-/blob/main/lib/default.nix

    nixosConfigurations = {
      ## cf https://gitlab.com/putchar/dotnix/-/blob/main/lib/mkNixosSystem.nix
      desktop = myLib.mkNixosSystem {
        hostname = "desktop";
        extraModules = [
          ## for now we declare lix here and see where it goes
          inputs.lix-module.nixosModules.default

          inputs.nixos-hardware.nixosModules.common-pc-ssd
          inputs.nixos-hardware.nixosModules.common-cpu-intel
        ];
      };

      framework = myLib.mkNixosSystem {
        hostname = "framework";
        extraModules = [
          inputs.nixos-hardware.nixosModules.framework
        ];
      };

      hetzner-1 = myLib.mkNixosSystem {
        hostname = "hetzner-1";
      };

      appenin = myLib.mkNixosSystem {
        hostname = "appenin";
        extraModules = [
          inputs.nixos-hardware.nixosModules.dell-xps-13-9310
        ];
      };

      s3ns = myLib.mkNixosSystem {
        hostname = "s3ns";
        extraModules = [
          #inputs.nixos-hardware.nixosModules.common-pc
          #inputs.nixos-hardware.nixosModules.common-pc-laptop
          #inputs.nixos-hardware.nixosModules.common-pc-ssd
          #inputs.nixos-hardware.nixosModules.common-cpu-intel
          #inputs.nixos-hardware.nixosModules.common-pc-laptop-acpi_call
          #inputs.nixos-hardware.nixosModules.hp-elitebook-2560p
        ];
      };
    };

    # Easier `nix build`-ing of configurations
    appenin = self.nixosConfigurations.appenin.config.system.build.toplevel;
    "putchar@appenin" = self.homeConfigurations."putchar@appenin".activationPackage;

    homeConfigurations = {
      useGlobalPkgs = true;
      useUserPackages = true;

      ## cf https://gitlab.com/putchar/dotnix/-/blob/main/lib/mkHomeManagerConfiguration.nix
      "putchar@desktop" = myLib.mkHomeManagerConfiguration {
        hostname = "desktop";
      };

      "putchar@framework" = myLib.mkHomeManagerConfiguration {
        hostname = "framework";
      };

      "putchar@s3ns" = myLib.mkHomeManagerConfiguration {
        hostname = "s3ns";
      };

      "putchar@appenin" = myLib.mkHomeManagerConfiguration {
        hostname = "appenin";
      };
    };

    deploy = {
      magicRollback = true;
      autoRollback = true;

      nodes = {
        hetzner-1 = {
          hostname = "hetzner-1";
          sshUser = "putchar";
          profiles.system = {
            ### root is the user who will be used to activate the derivation,
            ### not the one we use to ssh to
            ### once the deploy start. deploy-rs will do `ssh ${sshUser}` then `sudo -u ${user}`
            user = "root";
            path =
              inputs.deploy-rs.lib.x86_64-linux.activate.nixos
              self.nixosConfigurations.hetzner-1;
          };
        };
      };
    };
    # This is highly advised, and will prevent many possible mistakes
    checks =
      builtins.mapAttrs
      (system: deployLib: deployLib.deployChecks self.deploy)
      inputs.deploy-rs.lib;
  };
}
