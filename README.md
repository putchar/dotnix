---
marp: true
title: Marp
paginate: true
author: Slim Cadoux
theme:
class: lead
---


# My DotFiles

This is a flake with most (if not all) of my machines
I will use this README.md to explain some stuff that will be pushed in my blog in the near future.
I will also explain how to make use of this flake

---

### overriding
https://nixos.org/manual/nixpkgs/stable/#chap-overrides
#### pkgs.override
to override an argument of a function / derivation

```nix
let
  nixpkgs-version = "21.11";
  pkgs =
    import (fetchTarball
      "https://github.com/NixOS/nixpkgs/archive/nixos-${nixpkgs-version}.tar.gz") {};
in
  pkgs.mkShell {
    buildInputs = [
      # leiningen comes with jdk17 on default
      (pkgs.leiningen.override {jdk = pkgs.jdk11;})
    ];
  }
```
---

#### pkgs.overrideAttrs
to override an attribute set passed to a stdenv.mkDerivation

```nix
# https://nixos.org/manual/nixpkgs/stable/#sec-pkg-overrideAttrs
  services.pipewire = {
    package = pkgs.pipewire.overrideAttrs (oldAttrs: rec {
      src = pkgs.fetchFromGitLab {
        owner = "pipewire";
        repo = "pipewire";
        rev = "64cf5e80e6240284e6b757907b900507fe56f1b5";
        sha256 = "sha256-OMFtHduvSQNeEzQP+PlwfhWC09Jb8HN4SI42Z9KpZHE=";
      };
    });
```

---

#### on the use of `let / in` to factorise some stuff
```nix
{

inputs = {
    ## https://github.com/NixOS/nixpkgs/tree/nixos-unstable
    nixos-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
};

  outputs = inputs @ {self, ...}: let

    system = "x86_64-linux";
    ## smaller `pkgs` string attribute for devShell
    pkgs = inputs.nixos-unstable.legacyPackages.${system};
  in {
    devShells.${system}.default = pkgs.mkShell {
      name = "putchar-dotfile";
      packages = [
        pkgs.vim
        pkgs.git
      ];
    };
  };
}
```

---

#### is the same as
```nix
{

inputs = {
    ## https://github.com/NixOS/nixpkgs/tree/nixos-unstable
    nixos-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
};

  outputs = inputs @ {self, ...}:  {
    devShells.x86_64-linux.default = inputs.nixos-unstable.legacyPackages.x86_64-linux.mkShell {
      name = "putchar-dotfile";
      packages = [
        inputs.nixos-unstable.legacyPackages.x86_64-linux.vim
        inputs.nixos-unstable.legacyPackages.x86_64-linux.git
      ];
    };
  };
}
```

Way less elegant don't you think ?

---

#### use `let / in` with nixosConfiguration

```nix
{
  inputs = {
      nixos-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
      nixos-hardware.url = "github:NixOS/nixos-hardware/master";
      nixos-hardware.inputs.nixpkgs.follows = "nixos-unstable";
  };

  outputs = inputs @ {self, ...}: {
    nixosConfigurations = {
      desktop = inputs.nixos-unstable.lib.nixosSystem {
        system = "x86_64-linux";
        modules =
          [
            {_module.args = {inherit inputs;};}
            ./hardware-configuration.nix
            ./configuration.nix
            inputs.nixos-hardware.nixosModules.common-pc-ssd
            inputs.nixos-hardware.nixosModules.common-cpu-intel
          ];
      };
    };
  };
}
```

---
```nix
{
  inputs = {
      nixos-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
      nixos-hardware.url = "github:NixOS/nixos-hardware/master";
      nixos-hardware.inputs.nixpkgs.follows = "nixos-unstable";
  };

  outputs = inputs @ {self, ...}: {
    nixosConfigurations = let
      mkNixosSystem = {
        system ? "x86_64-linux",
        hostname,
        extraModules ? [],
      }:
        inputs.nixos-unstable.lib.nixosSystem {
          system = system;
          modules =
            [
              {_module.args = {inherit inputs;};}
              ./NixOS/modules
              ./NixOS/hosts/${hostname}/configuration.nix
            ]
            ++ extraModules;
        };
    in {
          desktop = self.myLib.mkNixosSystem {
            hostname = "desktop";
            extraModules = [
              inputs.nixos-hardware.nixosModules.common-pc-ssd
              inputs.nixos-hardware.nixosModules.common-cpu-intel
            ];
        };
      };
  };
}
```
