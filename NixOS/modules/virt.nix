{
  config,
  pkgs,
  lib,
  inputs,
  ...
}: let
  cfg = config.customNixOSModules.virt;
in {
  options.customNixOSModules.virt = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable virt globally or not
      '';
    };
  };

  ## TODO https://github.com/NixOS/nixpkgs/pull/164140
  config = lib.mkIf cfg.enable {
    virtualisation = {
      libvirtd = {
        enable = true;
        #package = inputs.nixos-stable.legacyPackages.x86_64-linux.libvirt;
        qemu = {
          #package = pkgs.qemu_kvm;
          ovmf.enable = true;
          ovmf.packages = [
            #pkgs.OVMF.fd
            pkgs.OVMFFull.fd
            ## cf https://github.com/NixOS/nixpkgs/issues/164064
            #inputs.edk2-202108.legacyPackages.x86_64-linux.OVMFFull.fd
            #inputs.ovmf.legacyPackages.x86_64-linux.OVMFFull.fd
          ];
          swtpm.enable = true;
          runAsRoot = true;
        };
      };
    };

    environment.etc = {
      "ovmf/edk2-x86_64-secure-code.fd" = {
        #source = config.virtualisation.libvirtd.qemu.package + "/share/qemu/edk2-x86_64-secure-code.fd";
        source = inputs.nixos-oldStable.legacyPackages.x86_64-linux.qemu + "/share/qemu/edk2-x86_64-secure-code.fd";
      };

      # we just make a flat copy of the file to use as a template for nvram storage
      # https://search.nixos.org/options?channel=unstable&show=environment.etc.%3Cname%3E.mode&from=0&size=50&sort=relevance&type=packages&query=etc
      "ovmf/edk2-i386-vars.fd" = {
        #source = config.virtualisation.libvirtd.qemu.package + "/share/qemu/edk2-i386-vars.fd";
        source = inputs.nixos-oldStable.legacyPackages.x86_64-linux.qemu + "/share/qemu/edk2-i386-vars.fd";
        mode = "0644";
        user = "libvirtd";
      };
    };

    systemd.tmpfiles.rules = [
      "f /dev/shm/looking-glass 0660 putchar libvirtd -"
    ];

    environment.sessionVariables.VAGRANT_DEFAULT_PROVIDER = ["libvirt"];
    environment.sessionVariables.LIBVIRT_DEFAULT_URI = ["qemu:///system"];
    environment.systemPackages = [
      pkgs.virt-manager
      pkgs.hwloc
      ## for better gaming experience
      pkgs.looking-glass-client
      ## issue with vagrant cf https://github.com/NixOS/nixpkgs/issues/211153
      pkgs.vagrant
      #inputs.nixos-stable.legacyPackages.x86_64-linux.vagrant

      pkgs.dive
      (
        pkgs.writeShellScriptBin "iommu.sh" ''
          shopt -s nullglob
          for IOMMU_GROUP in /sys/kernel/iommu_groups/*; do
              ${pkgs.coreutils}/bin/echo "IOMMU Group ''${IOMMU_GROUP##*/}:"
              for DEVICE in ''$IOMMU_GROUP/devices/*; do
                  ${pkgs.coreutils}/bin/echo -e "\t''$(${pkgs.pciutils}/bin/lspci -nns ''${DEVICE##*/})"
              done;
          done
        ''
      )
      (
        pkgs.writeShellScriptBin "usb.sh" ''
          for USB_CTRL in /sys/bus/usb/devices/usb*; do
              PCI_PATH="''$(${pkgs.coreutils}/bin/dirname "''$(${pkgs.coreutils}/bin/realpath "''${USB_CTRL}")")";
              ${pkgs.coreutils}/bin/echo "Bus ''$(${pkgs.coreutils}/bin/cat "''${USB_CTRL}/busnum") --> ''$(${pkgs.coreutils}/bin/basename ''$PCI_PATH) (IOMMU group ''$(${pkgs.coreutils}/bin/basename ''$(${pkgs.coreutils}/bin/realpath ''$PCI_PATH/iommu_group)))";
              ${pkgs.usbutils}/bin/lsusb -s "''$(${pkgs.coreutils}/bin/cat "''${USB_CTRL}/busnum"):";
              ${pkgs.coreutils}/bin/echo;
          done
        ''
      )
    ];
    #systemd.services.libvirtd.path = [pkgs.swtpm];
    users.users.putchar = {
      extraGroups = ["libvirtd" "kvm" "qemu-libvirtd"];
    };
  };
}
