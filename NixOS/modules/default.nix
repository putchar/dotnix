{
  config,
  pkgs,
  lib,
  ...
}: {
  imports = [
    ./audio.nix
    ./backlight.nix
    ./bluetooth.nix
    ./docker.nix
    ./i3.nix
    ./laptopProfile.nix
    ./media.nix
    ./motd.nix
    ./networkManager.nix
    ./nixUnstable.nix
    ./openvpn.nix
    ./pgadmin.nix
    ./pipewire.nix
    ./sway.nix
    ./vbox.nix
    ./virt.nix
    ./wireguard.nix
  ];

  # Edit this configuration file to define what should be installed on
  # your system.  Help is available in the configuration.nix(5) man page
  # and in the NixOS manual (accessible by running ‘nixos-help’).
  nix.settings = {
    sandbox = true;
    trusted-users = ["root" "@wheel" "putchar"];
    auto-optimise-store = true;
    extra-substituters = [
      "https://cache.lix.systems"
    ];

    trusted-public-keys = [
      "cache.lix.systems:aBnZUw8zA7H35Cz2RyKFVs3H4PlGTLawyY5KRbvJR8o="
    ];
  };

  nix.gc.automatic = true;
  nix.extraOptions = ''
    min-free = ${toString (100 * 1024 * 1024)}
    max-free = ${toString (1024 * 1024 * 1024)}
    keep-outputs = true
    keep-derivations = true
  '';

  #boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.tmp.cleanOnBoot = true;
  boot.tmp.useTmpfs = false;

  ## for iotop to work properly
  boot.kernel.sysctl = {
    "kernel.task_delayacct" = 1;
    "vm.max_map_count" = 262144;
  };
  # Set your time zone.
  time.timeZone = "Europe/Paris";

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  # i18n.defaultLocale = "en_US.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  # };

  nixpkgs.config.allowUnfree = true;
  services.fstrim.enable = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = [
    pkgs.vim
    # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    pkgs.wget
    pkgs.curl
    pkgs.tree
    pkgs.ncdu
    pkgs.gdu
    pkgs.htop
    pkgs.molly-guard
    pkgs.openssl
    pkgs.sysfsutils
    pkgs.file
    pkgs.usbutils
    pkgs.pciutils
    pkgs.dmidecode
    pkgs.lsb-release
    pkgs.lshw
    pkgs.unzip
    pkgs.zstd
    pkgs.gptfdisk
    pkgs.bash-completion
    (lib.hiPrio pkgs.bashInteractive)
    pkgs.neofetch
    pkgs.nvd
    pkgs.nixos-option
    (
      pkgs.writeShellScriptBin "motherboard_spec.sh" ''
        sudo ${pkgs.dmidecode}/bin/dmidecode -t 2
        ${pkgs.coreutils}/bin/echo "Bios Version: $(sudo ${pkgs.dmidecode}/bin/dmidecode -s bios-version)"
      ''
    )
    #(pkgs.ventoy-bin.override {withGtk3 = true;})

    (
      pkgs.writeShellScriptBin "remoteConnection" ''
        if [ $# -eq 0 ]
        then
          echo "No arguments provided"
          exit 1
        else
          exec ${pkgs.freerdp}/bin/xfreerdp /f /u:slim.cadoux /d:oscaroad.com /v:$@
        fi
      ''
    )
    pkgs.waynergy
  ];

  environment.etc."current-system-packages".text = let
    packages = builtins.map (p: "${p.name}") config.environment.systemPackages;
    sortedUnique = builtins.sort builtins.lessThan (lib.unique packages);
    formatted = builtins.concatStringsSep "\n" sortedUnique;
  in
    formatted;

  ### CHECK THIS !!
  # environment.etc."zsh/zshrc".source = "${pkgs.awscli}/share/zsh/site-functions/aws_zsh_completer.sh";

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:
  services.logind.extraConfig = ''
    RuntimeDirectorySize=8G
  '';
  services.udev.extraRules = ''
    KERNEL=="hidraw*", SUBSYSTEM=="hidraw", MODE="0666", TAG+="uaccess", TAG+="udev-acl"'';
  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  users.users.putchar.openssh.authorizedKeys.keys = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDcWK90+oiob+iK3S7CxpGDwwg5UXR0aw13y9G7jyh0Outpoc2FR7sqc627QgZ7PL85LLhWedWUHhUObLqf45SvpqLaQZ/0K0lh7ZRPrm0H0S77PuWsDBpwp8FLrGBCQCvs/3E9J2eg3Yr+XixXBguxSzDwc+bF60CQjmN0D98c63/np9NpWAzr+f+KCVJtj5xtYPgNa8wJsN6EfjMxy0SNxoorQUzG5qu5YB7/YyEToJYeSM2+gS1yXUoTsThYNEg/MLa93zugkX48jsEfKvpT/guKQtdSe9BVosfwRwXWUoq5aXGEjwfho2TkfWsIp9dffN29exxJrgErTSaqhly1K1DWIwG9bR9lr5C4t+aIppLokNaIephoQqbuf5p6PqV+vFO9+KZ6Ua2GXBKKcvCvDr2KIxe6kjRFCusJxgHG5BBEuNVQfgaJnk9aZeOgfzrgYuiRK9ZiMCzLFCi2uF5EALR30G0zNPG9Lk7lsRL+1E34aTP7Ad+5kfEXAYO2ZuM= putchar@desktop"
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDEuWEDaKM0qVQIJbe3/0x0/3nGLgt4Ilt08mOlXCBQW1DjGV7R9cbHzkSO5MY7qRTsDriVjrivQOd7BwpMUK+7QaRi5TnHZiS4h4P6PJA8/uZjSlSJZydjlGMLYG9eU0ad6a/ixuDHAgX1mrkKImq0CZZ0JOt9izSY1cUx2COBlnqU8sF0lZc+ilz6vECKhClCMN8xH6mLlFPUe0gUtCcfz4gT8gL29+zMg+aom+bU5VUgF5TvsDCb/HqMhIvzGAnW5PZe2PLkrcRPCq/vX9vRVtgKSMxkaLjZ/NHp/AJfeYecK3IMGUQMqefU6ZZn6Dbg6N6eYJ2H71pd4d2+29LRWXqJHAr94VnvP+teYdzeR7I95PKl+rlP3tKONiam4meFG5ja2KughCxopFDpEg7KXPswyOuH42drXaEuNG2MVm73iGySicJ/9S3g5l0ikoOCY/6eaS1kk8GnofV9RAOjM9YAxMSdSuWGfUrVEqDl07w3aQE6lZ7WeQUo1JMTgeM= slim.cadoux@t480"
  ];
  programs.ssh.startAgent = true;

  # Open ports in the firewall.
  #networking.firewall.allowedTCPPorts = [ 24800 ];
  #networking.firewall.allowedUDPPorts = [ 24800 ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;
  networking.extraHosts = ''
    94.130.143.34   hetzner-1
    192.168.1.10    rpi00
    192.168.1.11    rpi01
    192.168.1.12    rpi02
    192.168.1.13    rpi03
    192.168.1.199   laptop
    192.168.1.23    framework
    192.168.1.72    desktop
    192.168.1.60    appenin
    192.168.122.12  nixos-vm
  '';

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.putchar = {extraGroups = ["dialout"];};

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05";
  # Did you read the comment?
}
