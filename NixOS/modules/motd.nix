{config, ...}: {
  users.motd = ''
    Welcome
    - This machine is managed with nix and deploy-rs

    Hostname: ${config.networking.hostName}
    OS:       NixOS ${config.system.nixos.release} (${config.system.nixos.codeName})
    Version:  ${config.system.nixos.version}
    Kernel:   ${config.boot.kernelPackages.kernel.version}
  '';
}
