{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customNixOSModules.laptopProfile;
in {
  options.customNixOSModules.laptopProfile = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable laptopProfile globally or not
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    services.fwupd.enable = true;
    services.thermald.enable = true;

    powerManagement = {
      enable = true;
      powertop.enable = true;
      #cpuFreqGovernor = lib.mkDefault "powersave";
      #cpuFreqGovernor = lib.mkDefault "ondemand";
    };
    # https://github.com/AdnanHodzic/auto-cpufreq
    #services.auto-cpufreq.enable = true;

    ## https://linrunner.de/tlp/settings/index.html
    services.tlp = {
      enable = true;
      settings = {
        #PCIE_ASPM_ON_BAT = "powersupersave";
        #CPU_ENERGY_PERF_POLICY_ON_BAT = "power";

        CPU_MAX_PERF_ON_BAT = 80;
        #CPU_MAX_PERF_ON_AC = 80;

        #CPU_SCALING_GOVERNOR_ON_AC = "powersave";
        CPU_SCALING_GOVERNOR_ON_BAT = "powersave";

        START_CHARGE_THRESH_BAT0 = 30;
        STOP_CHARGE_THRESH_BAT0 = 80;

        START_CHARGE_THRESH_BAT1 = 30;
        STOP_CHARGE_THRESH_BAT1 = 80;
      };
    };

    #services.upower.enable = true;
    environment.systemPackages = [
      pkgs.powertop
      pkgs.upower
    ];
  };
}
