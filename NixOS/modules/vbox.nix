{
  config,
  pkgs,
  lib,
  inputs,
  ...
}: let
  cfg = config.customNixOSModules.vbox;
in {
  options.customNixOSModules.vbox = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable virtualbox globally or not
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    virtualisation.virtualbox.host = {
      enable = true;
      enableExtensionPack = false;
    };
    users.extraGroups.vboxusers.members = ["putchar"];
  };
}
