{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customNixOSModules.pgadmin;
  secretPath = "/home/putchar/Documents/gitlab.com/putchar/dotnix/NixOS/secrets";
in {
  options.customNixOSModules.pgadmin = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable pgadmin globally or not
      '';
    };
  };
  config = lib.mkIf cfg.enable {
    services.pgadmin.enable = true;
    services.pgadmin.initialEmail = "slim.cadoux@gmail.com";
    services.pgadmin.initialPasswordFile = "${secretPath}/pgadmin.txt";
  };
}
