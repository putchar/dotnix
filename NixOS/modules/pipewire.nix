{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customNixOSModules.pipewire;
in {
  options.customNixOSModules.pipewire = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable pipewire globally or not
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    users.users.putchar = {extraGroups = ["audio"];};

    #environment.systemPackages = [
    #  pkgs.helvum
    #];

    # disable default sound config and make sure it is disabled
    # as we use pipewire.
    sound.enable = false;
    services.pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      wireplumber.enable = true;
    };
  };
}
