{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customNixOSModules.openvpn;
  openvpnPkcs11Support = pkgs.openvpn.override {pkcs11Support = true;};
in {
  options.customNixOSModules.openvpn = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable openvpn globally or not
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    ## this will allow us to "get" path of opensc-pkcs11.so for instance
    environment.pathsToLink = ["/libexec"];
    services.udev.packages = [pkgs.yubikey-personalization]; # or we cant talk to OTP from userland

    services.resolved = {
      enable = true;
      dnssec = "false";
      fallbackDns = ["8.8.8.8" "8.8.4.4" "1.1.1.1"];
    };

    ## tailscale stuff
    services.tailscale.enable = true;
    environment.systemPackages = [
      ## cf https://github.com/NixOS/nixpkgs/blob/nixos-unstable/pkgs/tools/networking/openvpn/default.nix#L12
      #(pkgs.openvpn.override {pkcs11Support = true;})
      openvpnPkcs11Support
      pkgs.yubikey-manager-qt
      pkgs.opensc
      (
        pkgs.writeShellScriptBin "openvpn_show_pkcs11_ids.sh" ''
          ${openvpnPkcs11Support}/bin/openvpn --show-pkcs11-ids ${pkgs.opensc}/lib/pkcs11/opensc-pkcs11.so
        ''
      )
      (
        pkgs.writeShellScriptBin "launchVPNDev" ''
          sudo openvpn --config /home/putchar/Documents/gitlab.com/putchar/dotnix/secret/officeVPNDev.conf
        ''
      )
      (
        pkgs.writeShellScriptBin "launchVPNLab" ''
          sudo openvpn --config /home/putchar/Documents/gitlab.com/putchar/dotnix/secret/officeVPNLab.conf
        ''
      )
    ];
    services.pcscd = {
      enable = true;
      plugins = [
        pkgs.ccid
      ];
    };
  };
}
