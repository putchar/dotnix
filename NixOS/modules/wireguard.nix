{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customNixOSModules.wireguard;
  secretPath = "/home/putchar/Documents/gitlab.com/putchar/dotnix/NixOS/secrets";
  immo1Ip = "51.75.242.101";
in {
  options.customNixOSModules.wireguard = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable wireguard globally or not
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    networking.wg-quick.interfaces = {
      scaleway-qa = {
        address = ["10.51.0.2/24"];
        #dns = [ "10.42.3.1" ];
        privateKeyFile = "${secretPath}/immo-3.pvt.key";
        peers = [
          {
            publicKey = "PnGkhA0ABknwEnsB+U5PJFS3JCGhGkzqjRzpcxpb2BQ=";
            presharedKeyFile = "${secretPath}/immo-3.preshared.key";
            #allowedIPs = [ "10.42.2.0/24" "172.17.202.184/32" ];
            allowedIPs = ["10.42.3.0/24"];
            endpoint = "51.15.235.25:51820";
            persistentKeepalive = 25;
          }
        ];
      };

      scaleway-prod = {
        address = ["10.52.0.2/24"];
        privateKeyFile = "${secretPath}/scaleway.pvt.key";
        #dns = ["10.42.4.1"];
        peers = [
          {
            publicKey = "6XXN3UWvxdhVsV0/4Ios1x4JzblW6PDXONHOIXlZYzA=";
            presharedKeyFile = "${secretPath}/scaleway.preshared.key";
            #allowedIPs = ["10.42.4.0/24" "10.42.4.1/32"];
            allowedIPs = ["10.42.4.0/24" "10.42.5.0/24"];
            endpoint = "51.15.237.157:51820";
            persistentKeepalive = 25;
          }
        ];
      };
    };
  };
}
