{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customNixOSModules.backlight;
in {
  options.customNixOSModules.backlight = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable audio globally or not
      '';
    };
  };

  ## https://nixos.wiki/wiki/Backlight
  config = lib.mkIf cfg.enable {
    programs.light.enable = true;
    environment.systemPackages = [
      pkgs.brightnessctl
    ];

    users.users.putchar = {
      extraGroups = ["video"];
    };
  };
}
