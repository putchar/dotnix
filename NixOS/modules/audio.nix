{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customNixOSModules.audio;
in {
  options.customNixOSModules.audio = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable audio globally or not
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    users.users.putchar = {extraGroups = ["audio"];};

    # Enable sound.
    sound.enable = true;
    hardware.pulseaudio = {
      enable = true;
      support32Bit = true;
      package = pkgs.pulseaudioFull;
      # if full package is needed
      extraModules = [
        pkgs.pulseaudio-modules-bt
      ];
    };
  };
}
