{
  config,
  pkgs,
  lib,
  inputs,
  ...
}: let
  cfg = config.customNixOSModules.bluetooth;
in {
  options.customNixOSModules.bluetooth = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable bluetooth globally or not
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    hardware.bluetooth = {
      enable = true;
      ## cf https://github.com/NixOS/nixpkgs/pull/170194
      #package = inputs.bluez.legacyPackages.x86_64-linux.bluez;
      #package = inputs.nixos-stable.legacyPackages.x86_64-linux.bluez;
    };

    services.blueman.enable = true;
  };
}
