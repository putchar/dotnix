{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customNixOSModules.sway;
  waylandEnv = {
    CLUTTER_BACKEND = "wayland";
    SDL_VIDEODRIVER = "wayland";
    QT_QPA_PLATFORM = "wayland";
    QT_WAYLAND_DISABLE_WINDOWDECORATION = "1";
    _JAVA_AWT_WM_NONREPARENTING = "1";
    MOZ_ENABLE_WAYLAND = "1";
    XDG_SESSION_TYPE = "wayland";
    XDG_SESSION_DESKTOP = "sway";
    XDG_CURRENT_DESKTOP = "sway";
    WLR_NO_HARDWARE_CURSORS = "1";
    #NIXOS_OZONE_WL = "1";
    #GTK_USE_PORTAL = "1";
  };
in {
  ## https://arewewaylandyet.com/
  options.customNixOSModules.sway = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable sway globally or not
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    ## documentation is available
    ## cf https://nixos.wiki/wiki/Sway
    ## cf https://man.sr.ht/~kennylevinsen/greetd/
    ## cf https://wiki.archlinux.org/title/Greetd
    ## cf https://gist.github.com/ghostface/ed095be58730e2164f7ce47dd218a281
    ## cf https://github.com/KubqoA/dotfiles
    ## cf https://github.com/demus07/sway-dots/blob/main/configuration.nix
    boot.kernelParams = ["console=tty1"];

    services.greetd = {
      enable = true;
      restart = false;
      vt = 7;
      settings = {
        default_session = {
          ## if tuigreet does not work anymore we can still switch to agreety
          #command =
          #  "${pkgs.greetd.greetd}/bin/agreety --cmd ${pkgs.sway}/bin/sway";
          command = ''
            ${pkgs.greetd.tuigreet}/bin/tuigreet --time --asterisks --cmd ${pkgs.sway}/bin/sway
          '';
          user = "greeter";
        };
      };
    };
    qt.platformTheme = "qt5ct";
    xdg.portal = {
      enable = true;
      wlr.enable = true;
      #gtkUsePortal = true;
      extraPortals = [
        #pkgs.xdg-desktop-portal
        #pkgs.xdg-desktop-portal-wlr
        pkgs.xdg-desktop-portal-gtk
        #pkgs.xdg-desktop-portal-gnome
      ];
    };

    environment.etc."greetd/environments".text = "sway";
    environment.variables = waylandEnv;
    environment.sessionVariables = waylandEnv;

    environment.pathsToLink = ["/libexec"];
    services.gvfs.enable = true;
    # Mount, trash, and other functionalities
    services.tumbler.enable = true;
    # Thumbnail support for images
    programs.sway = {
      enable = true;
      extraPackages = [
        pkgs.swaylock
        # lockscreen
        pkgs.swayidle
        pkgs.swaybg
        pkgs.waybar
        # status bar
        pkgs.kanshi
        # autorandr
        pkgs.greetd.tuigreet
        pkgs.swappy
        pkgs.grim
        pkgs.slurp
        #xdg-desktop-portal
        #xdg-desktop-portal-wlr
        #xdg-desktop-portal-gtk
        pkgs.numix-gtk-theme
        pkgs.numix-icon-theme-square
        pkgs.wl-clipboard
        pkgs.alacritty
        pkgs.wofi
        pkgs.gtk-engine-murrine
        pkgs.gtk_engines
        pkgs.gsettings-desktop-schemas
      ];
      extraSessionCommands = ''
        export CLUTTER_BACKEND="wayland"
        export SDL_VIDEODRIVER="wayland"
        export QT_QPA_PLATFORM="wayland"
        export QT_WAYLAND_DISABLE_WINDOWDECORATION="1"
        export _JAVA_AWT_WM_NONREPARENTING="1"
        export MOZ_ENABLE_WAYLAND="1"
        export XDG_SESSION_TYPE="wayland"
        export XDG_SESSION_DESKTOP="sway"
        export XDG_CURRENT_DESKTOP="sway"
        export WLR_NO_HARDWARE_CURSORS="1"
        #export NIXOS_OZONE_WL="1"
        #export GTK_USE_PORTAL="1"
      '';
    };
  };
}
