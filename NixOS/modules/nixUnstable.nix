{
  config,
  pkgs,
  inputs,
  lib,
  ...
}: let
  cfg = config.customNixOSModules.nixUnstable;
  base = "/etc/nixpkgs/channels";
  nixpkgsPath = "${base}/nixpkgs";
  #nixpkgs2111Path = "${base}/nixpkgs2111";
in {
  options.customNixOSModules.nixUnstable = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable nixUnstable globally or not
      '';
    };
  };

  ## cf https://github.com/NobbZ/nixos-config/blob/master/nixos/modules/flake.nix
  config = lib.mkIf cfg.enable {
    nix = {
      ## https://search.nixos.org/packages?channel=unstable&from=0&size=50&sort=relevance&type=packages&query=nix
      ## https://search.nixos.org/packages?channel=unstable&show=nixVersions.unstable&from=0&size=50&sort=relevance&type=packages&query=nixVersion
      #package = pkgs.nixVersions.nix_2_6;
      ## this is latest nix stable
      #package = inputs.nixos-unstable.legacyPackages.x86_64-linux.nixVersions.nix_2_20;
      #package = inputs.nix.packages.${pkgs.system}.nix;
      extraOptions = ''
        experimental-features = nix-command flakes
      '';
      registry.nixpkgs.flake = inputs.nixos-unstable;
      #registry.nixpkgs.flake = nixpkgs;

      nixPath = [
        "nixpkgs=${nixpkgsPath}"
        #"nixpkgs=${nixpkgsPath}"
        #"/nix/var/nix/profiles/per-user/root/channels"
      ];
    };

    systemd.tmpfiles.rules = [
      "L+ ${nixpkgsPath}     - - - - ${inputs.nixos-unstable}"
      #"L+ ${nixpkgs2111Path} - - - - ${nixpkgs-2111}"
    ];
  };
}
