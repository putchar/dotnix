{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customNixOSModules.i3;
in {
  options.customNixOSModules.i3 = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable i3 globally or not
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    # Configure keymap in X11
    services.xserver.layout = "us";
    services.xserver.xkbVariant = "intl";
    # services.xserver.xkbOptions = "eurosign:e";
    # Enable touchpad support (enabled default in most desktopManager).
    services.xserver.libinput.enable = true;
    services.gnome.gnome-keyring.enable = true;
    services.gvfs.enable = true;

    services.xserver = {
      enable = true;
      desktopManager = {xterm.enable = false;};

      displayManager = {
        defaultSession = "none+i3";
        lightdm.enable = true;
        lightdm.greeters.gtk.theme.name = "Numix";
        lightdm.greeters.gtk.iconTheme.name = "Numix-Square";
        lightdm.greeters.gtk.extraConfig = ''
          font-name = Sans Bold 10
          xft-antialias = true
          xft-dpi = 96
          xft-rgba = rgb
          xft-hintstyle = hintslight
          hide-user-image = true
          position = 30%,end
          clock-format = %a: %d-%m-%Y %H:%M:%S
        '';
        #sessionCommands = ''
        #  xrandr --output DP-1 --mode 1920x1080 --pos 1920x0 --rotate normal --output HDMI-1 --off --output HDMI-2 --primary --mode 1920x1080 --pos 0x0 --rotate normal
        #'';
      };
      windowManager.i3 = {
        enable = true;
        package = pkgs.i3-gaps;
        extraPackages = [
          pkgs.rofi
          pkgs.i3status
          # gives you the default i3 status bar
          pkgs.i3lock
          # default i3 screen locker
          pkgs.i3blocks
          # if you are planning on using i3blocks over i3status
          pkgs.alacritty
          pkgs.arandr
          pkgs.lxappearance
          pkgs.numix-gtk-theme
          pkgs.numix-icon-theme-square
          pkgs.gtk-engine-murrine
          pkgs.gtk_engines
          pkgs.gsettings-desktop-schemas
          pkgs.flameshot
          pkgs.ntfs3g
          pkgs.xorg.xev
          pkgs.lm_sensors
          pkgs.gparted
        ];
      };
    };

    #services.gvfs = {
    #  enable = true;
    #  package = lib.mkForce pkgs.gnome3.gvfs;
    #};

    #security.pam.services.putchar.pamMount = true;
    environment.pathsToLink = [
      "/libexec"
    ];
    # links /libexec from derivations to /run/current-system/sw
    programs.dconf.enable = true;
    environment.sessionVariables.TERMINAL = ["alacritty"];
    environment.sessionVariables.TERM = ["xterm-256color"];
    environment.sessionVariables.EDITOR = ["vim"];
  };
}
