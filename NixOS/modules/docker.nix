{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customNixOSModules.docker;
in {
  options.customNixOSModules.docker = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable docker globally or not
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    virtualisation = {
      docker = {
        enable = true;
        package = pkgs.docker_24;
        daemon.settings = {
          bip = "192.168.100.254/24";
          fixed-cidr = "192.168.100.0/24";
          default-address-pool = [
            {
              base = "192.168.100.0/20";
              size = 24;
            }
          ];
          features = {
            buildkit = true;
          };
        };
        storageDriver = "overlay2";
        logDriver = "journald";
        liveRestore = false;
      };
    };

    networking.firewall.trustedInterfaces = ["docker0"];

    environment.systemPackages = [
      pkgs.docker-compose
      #pkgs.kind
      #pkgs.minikube
      #pkgs.docker-machine-kvm2
    ];

    users.users.putchar = {extraGroups = ["docker"];};
  };
}
