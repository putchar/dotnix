{
  config,
  pkgs,
  lib,
  inputs,
  ...
}: let
  cfg = config.customNixOSModules.media;
in {
  options.customNixOSModules.media = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable media stuff globally or not
      '';
    };
  };

  ## TODO https://github.com/NixOS/nixpkgs/pull/164140
  config = lib.mkIf cfg.enable {
    users.extraGroups.medias.members = ["putchar" "transmission" "sonarr" "radarr"];
    users.extraGroups.transmission.members = ["sonarr" "radarr"];

    ## jellyfin == media center
    ## http://localhost:8096/
    services.jellyfin.enable = true;

    ## radarr == movies
    ## http://localhost:7878/
    services.radarr.enable = true;

    ## sonarr == series
    ##  http://localhost:8989
    services.sonarr.enable = true;

    ## bazarr == subtitles
    ## http://localhost:6767/
    services.bazarr.enable = true;

    ## torrent client
    ## http://localhost:9091/
    services.transmission = {
      enable = true;
      #settings = {
      #  download-dir = "/srv/medias/transmission/";
      #  #incomplete-dir = "/srv/medias/transmission/.incomplete";
      #  #incomplete-dir-enabled = true;
      #};
    };

    systemd.tmpfiles.rules = [
      "d /srv/radarr 0755 radarr radarr"
      "d /srv/sonarr 0755 sonarr sonarr"
      "d /srv/bazarr 0755 bazarr bazarr"
    ];

    environment.etc."traefik/traefik.yml".text = ''

    '';
    ## reverse proxy
    services.traefik = {
      enable = true;
      staticConfigOptions = {
        entryPoints.web.address = ":80";
        http.routers.to-radarr.rule = "Host(`radar.local.net`)";
        http.routers.to-radarr.service = "radarr-service";

        http.services.radarr-service.loadBalancer.servers = [
          {
            url = "http://localhost:7878";
          }
          #{
          #  url = "http://localhost:7878";
          #}
        ];
      };
    };
  };
}
