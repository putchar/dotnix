{
  config,
  pkgs,
  lib,
  inputs,
  ...
}: {
  users.extraGroups.medias.members = ["putchar" "transmission" "sonarr" "radarr"];
  users.extraGroups.transmission.members = ["sonarr" "radarr"];

  ## jellyfin == media center
  ## http://localhost:8096/
  services.jellyfin.enable = true;

  ## radarr == movies
  ## http://localhost:7878/
  services.radarr.enable = true;

  ## sonarr == series
  ##  http://localhost:8989
  services.sonarr.enable = true;

  ## bazarr == subtitles
  ## http://localhost:6767/
  services.bazarr.enable = true;

  ## torrent client
  ## http://localhost:9091/
  services.transmission = {
    enable = true;
    settings = {
      download-dir = "/data/medias/transmission/Downloads";
      incomplete-dir = "/data/medias/transmission/.incomplete";
      incomplete-dir-enabled = true;
    };
  };

  systemd.tmpfiles.rules = [
    "d /data/medias/transmission/Downloads 0755 transmission transmission"
    "d /data/medias/transmission/.incomplete 0755 transmission transmission"
    "d /data/radarr 0755 radarr radarr"
    "d /data/sonarr 0755 sonarr sonarr"
    "d /data/bazarr 0755 bazarr bazarr"
  ];

  services.nginx = {
    enable = true;
    virtualHosts = {
      ## https://gist.github.com/Belphemur/47f76c40defef0269615
      "transmission.putchar.net" = {
        default = false;
        forceSSL = true;
        enableACME = true;
        locations."/" = {
          proxyPass = "http://localhost:9091";
          extraConfig = ''
            proxy_pass_header   X-Transmission-Session-Id;
          '';
        };
        extraConfig = ''
          proxy_hide_header X-Frame-Options; # this should allow iframe

          allow 78.197.157.127/32;
          deny all;

          access_log /var/log/nginx/transmission.putchar.net.access.log;
          error_log /var/log/nginx/transmission.putchar.net.error.log error;
        '';
      };

      "radarr.putchar.net" = {
        default = false;
        forceSSL = true;
        enableACME = true;
        locations."/" = {
          proxyPass = "http://localhost:7878";
        };
        extraConfig = ''
          allow 78.197.157.127/32;
          deny all;

          access_log /var/log/nginx/radarr.putchar.net.access.log;
          error_log /var/log/nginx/radarr.putchar.net.error.log error;
        '';
      };

      "jellyfin.putchar.net" = {
        default = false;
        forceSSL = true;
        enableACME = true;
        locations."/" = {
          proxyPass = "http://localhost:8096";
        };
        extraConfig = ''
          allow 78.197.157.127/32;
          deny all;

          access_log /var/log/nginx/jellyfin.putchar.net.access.log;
          error_log /var/log/nginx/jellyfin.putchar.net.error.log error;
        '';
      };
    };
  };
}
