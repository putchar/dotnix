{
  config,
  pkgs,
  inputs,
  ...
}:
#https://gist.github.com/SrTobi/ee6104514e2f14a0f1eae201654f2012
#https://ilanjoselevich.com/blog/building-websites-using-nix-flakes-and-zola/
#https://blog.matejc.com/blogs/myblog/nixos-hydra-nginx
{
  #  environment.systemPackages = [ blog.packages.x86_64-linux.website ];
  #services.logrotate.enable = true;
  #services.logrotate.paths = { nginx = { path = "/var/log/nginx/*.log"; }; };
  services.nginx = {
    enable = true;
    enableReload = true;
    # Use recommended settings
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;

    commonHttpConfig = ''
      # Add HSTS header with preloading to HTTPS requests.
      # Adding this header to HTTP requests is discouraged
      map $scheme $hsts_header {
          https   "max-age=31536000; includeSubdomains; preload";
      }
      add_header Strict-Transport-Security $hsts_header;

      # Enable CSP for your services.
      add_header Content-Security-Policy "script-src 'self'; object-src 'none'; base-uri 'none';" always;

      # Minimize information leaked to other domains
      #add_header 'Referrer-Policy' 'origin-when-cross-origin';
      add_header 'Referrer-Policy' 'same-origin';

      # Disable embedding as a frame
      add_header X-Frame-Options DENY;

      # Prevent injection of code in other mime types (XSS Attacks)
      add_header X-Content-Type-Options nosniff;

      # Enable XSS protection of the browser.
      # May be unnecessary when CSP is configured properly (see above)
      add_header X-XSS-Protection "1; mode=block";

      # This might create errors
      proxy_cookie_path / "/; secure; HttpOnly; SameSite=strict";
    '';

    virtualHosts = {
      "blog.putchar.net" = {
        default = false;
        forceSSL = true;
        enableACME = true;
        locations."/" = {return = "301 https://putchar.net/";};
        extraConfig = ''
          access_log /var/log/nginx/blog.putchar.net.access.log;
          error_log /var/log/nginx/blog.putchar.net.error.log error;
        '';
      };

      "putchar.net" = {
        default = true;
        forceSSL = true;
        enableACME = true;
        root = "${inputs.blog.packages.x86_64-linux.website}";
        extraConfig = ''
          access_log /var/log/nginx/putchar.net.access.log;
          error_log /var/log/nginx/putchar.net.error.log error;
        '';
      };
    };
  };
  # we allow both http and https
  networking.firewall.allowedTCPPorts = [80 443];

  # assertions
  # - You must define `security.acme.certs.<name>.email` or
  #   `security.acme.email` to register with the CA. Note that using
  #   many different addresses for certs may trigger account rate limits.

  # - You must accept the CA's terms of service before using
  #   the ACME module by setting `security.acme.acceptTerms`
  #   to `true`. For Let's Encrypt's ToS see https://letsencrypt.org/repository/
  #security.acme.email = "slim.cadoux@gmail.com";
  security.acme.defaults.email = "slim.cadoux@gmail.com";
  security.acme.acceptTerms = true;
}
