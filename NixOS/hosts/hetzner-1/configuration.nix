# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  config,
  pkgs,
  lib,
  inputs,
  ...
}: {
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ./nginx.nix
    ./media.nix
    #./wireguard.nix
  ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  #boot.loader.grub.version = 2;
  boot.loader.grub.efiSupport = false;
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.devices = ["/dev/disk/by-id/nvme-eui.00080d020021765e" "/dev/disk/by-id/nvme-eui.002538b711b90c38"];
  # or "nodev" for efi only

  boot.loader.grub.configurationLimit = 10;

  boot.swraid.mdadmConf = ''
    DEVICE /dev/nvme0n1p1 /dev/nvme1n1p1
    DEVICE /dev/nvme0n1p2 /dev/nvme1n1p2
    DEVICE /dev/sda /dev/sdb
    ARRAY /dev/md/0 metadata=0.90 UUID=a95932cb:3a836710:9f9a00b5:32533d6c
    ARRAY /dev/md/1 metadata=1.2  UUID=2d9a365a:5a7836db:488ce1ac:2bea79f6
    ARRAY /dev/md/2 metadata=1.2  UUID=a1a7f382:1fd68d24:deea484e:e4501363
  '';

  networking.hostName = "hetzner-1";
  # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Set your time zone.
  time.timeZone = "Europe/Paris";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  #networking.interfaces.enp0s31f6.useDHCP = true;
  networking.interfaces.enp0s31f6.ipv4.addresses = [
    {
      address = "94.130.143.34";
      prefixLength = 26;
    }
  ];

  networking.defaultGateway = "94.130.143.1";

  #networking.interfaces.enp0s31f6.ipv6.addresses = [
  #  {
  #    address = "2a01:4f8:13b:2d1d::/64";
  #    prefixLength = "64";
  #  }
  #];
  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  # i18n.defaultLocale = "en_US.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  # };

  # Enable the X11 windowing system.
  # services.xserver.enable = true;

  services.resolved = {
    enable = true;
    dnssec = "false";
    fallbackDns = ["185.12.64.1" "185.12.64.2" "8.8.8.8" "8.8.4.4" "1.1.1.1"];
  };

  virtualisation = {
    libvirtd = {
      enable = true;
      qemu = {
        ovmf.enable = true;
        swtpm.enable = true;
        ovmf.packages = [
          pkgs.OVMFFull.fd
        ];
      };
    };
  };

  security.polkit.enable = true;
  # Configure keymap in X11
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  # sound.enable = true;
  # hardware.pulseaudio.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  # users.users.jane = {
  #   isNormalUser = true;
  #   extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
  # };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = [
    pkgs.vim
    # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    pkgs.curl
    pkgs.wget
    pkgs.neofetch
    pkgs.htop
    pkgs.gotop
    pkgs.bottom
    pkgs.nvd
    pkgs.git
    pkgs.gnumake
    pkgs.tmux
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  nix.settings.trusted-users = ["root" "@wheel" "putchar"];
  nix.settings.sandbox = true;

  nixpkgs.config.allowUnfree = true;
  nix.package = pkgs.nixFlakes;
  nix.gc.automatic = true;
  nix.settings.auto-optimise-store = true;
  nix.extraOptions = ''
    min-free = ${toString (100 * 1024 * 1024)}
    max-free = ${toString (1024 * 1024 * 1024)}
    keep-outputs = true
    keep-derivations = true
    experimental-features = nix-command flakes
  '';

  environment.etc."current-system-packages".text = let
    packages = builtins.map (p: "${p.name}") config.environment.systemPackages;
    sortedUnique = builtins.sort builtins.lessThan (lib.unique packages);
    formatted = builtins.concatStringsSep "\n" sortedUnique;
  in
    formatted;

  # List services that you want to enable:
  services.fstrim.enable = true;
  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.openssh.settings.PermitRootLogin = "without-password";
  services.openssh.settings.LogLevel = "VERBOSE";

  services.fail2ban.enable = true;
  #services.fail2ban.package = inputs.nixos-oldStable.legacyPackages.x86_64-linux.fail2ban;

  services.fail2ban.ignoreIP = ["78.197.157.127/32"];

  users.users.root.openssh.authorizedKeys.keys = [
    # Replace with your public key
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDcWK90+oiob+iK3S7CxpGDwwg5UXR0aw13y9G7jyh0Outpoc2FR7sqc627QgZ7PL85LLhWedWUHhUObLqf45SvpqLaQZ/0K0lh7ZRPrm0H0S77PuWsDBpwp8FLrGBCQCvs/3E9J2eg3Yr+XixXBguxSzDwc+bF60CQjmN0D98c63/np9NpWAzr+f+KCVJtj5xtYPgNa8wJsN6EfjMxy0SNxoorQUzG5qu5YB7/YyEToJYeSM2+gS1yXUoTsThYNEg/MLa93zugkX48jsEfKvpT/guKQtdSe9BVosfwRwXWUoq5aXGEjwfho2TkfWsIp9dffN29exxJrgErTSaqhly1K1DWIwG9bR9lr5C4t+aIppLokNaIephoQqbuf5p6PqV+vFO9+KZ6Ua2GXBKKcvCvDr2KIxe6kjRFCusJxgHG5BBEuNVQfgaJnk9aZeOgfzrgYuiRK9ZiMCzLFCi2uF5EALR30G0zNPG9Lk7lsRL+1E34aTP7Ad+5kfEXAYO2ZuM= putchar@desktop"
  ];

  users.users.putchar = {
    isNormalUser = true;
    extraGroups = [
      "wheel"
      "kvm"
      "libvirtd"
      "qemu-libvirtd"
    ];
    # Enable ‘sudo’ for the user.
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDcWK90+oiob+iK3S7CxpGDwwg5UXR0aw13y9G7jyh0Outpoc2FR7sqc627QgZ7PL85LLhWedWUHhUObLqf45SvpqLaQZ/0K0lh7ZRPrm0H0S77PuWsDBpwp8FLrGBCQCvs/3E9J2eg3Yr+XixXBguxSzDwc+bF60CQjmN0D98c63/np9NpWAzr+f+KCVJtj5xtYPgNa8wJsN6EfjMxy0SNxoorQUzG5qu5YB7/YyEToJYeSM2+gS1yXUoTsThYNEg/MLa93zugkX48jsEfKvpT/guKQtdSe9BVosfwRwXWUoq5aXGEjwfho2TkfWsIp9dffN29exxJrgErTSaqhly1K1DWIwG9bR9lr5C4t+aIppLokNaIephoQqbuf5p6PqV+vFO9+KZ6Ua2GXBKKcvCvDr2KIxe6kjRFCusJxgHG5BBEuNVQfgaJnk9aZeOgfzrgYuiRK9ZiMCzLFCi2uF5EALR30G0zNPG9Lk7lsRL+1E34aTP7Ad+5kfEXAYO2ZuM= putchar@desktop"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDEuWEDaKM0qVQIJbe3/0x0/3nGLgt4Ilt08mOlXCBQW1DjGV7R9cbHzkSO5MY7qRTsDriVjrivQOd7BwpMUK+7QaRi5TnHZiS4h4P6PJA8/uZjSlSJZydjlGMLYG9eU0ad6a/ixuDHAgX1mrkKImq0CZZ0JOt9izSY1cUx2COBlnqU8sF0lZc+ilz6vECKhClCMN8xH6mLlFPUe0gUtCcfz4gT8gL29+zMg+aom+bU5VUgF5TvsDCb/HqMhIvzGAnW5PZe2PLkrcRPCq/vX9vRVtgKSMxkaLjZ/NHp/AJfeYecK3IMGUQMqefU6ZZn6Dbg6N6eYJ2H71pd4d2+29LRWXqJHAr94VnvP+teYdzeR7I95PKl+rlP3tKONiam4meFG5ja2KughCxopFDpEg7KXPswyOuH42drXaEuNG2MVm73iGySicJ/9S3g5l0ikoOCY/6eaS1kk8GnofV9RAOjM9YAxMSdSuWGfUrVEqDl07w3aQE6lZ7WeQUo1JMTgeM= slim.cadoux@t480"
    ];
  };
  security.sudo.wheelNeedsPassword = false;

  ## tailscale setup
  services.tailscale.enable = true;
  networking.firewall.checkReversePath = "loose";

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05";
  # Did you read the comment?
}
