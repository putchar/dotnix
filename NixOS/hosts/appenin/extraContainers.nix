{
  config,
  pkgs,
  ...
}: {
  config = {
    ## systemctl cat docker-konga.service
    virtualisation.oci-containers = {
      backend = "docker";
      containers = {
        konga = {
          image = "pantsel/konga:0.14.9";
          autoStart = false;
          ports = ["127.0.0.1:1337:1337"];
          #volumes = [
          #  "/root/hackagecompare/packageStatistics.json:/root/hackagecompare/packageStatistics.json"
          #];
          #cmd = [
          #  "--base-url"
          #  "\"/hackagecompare\""
          #];
        };
      };
    };

    ## https://blog.beardhatcode.be/2020/12/Declarative-Nixos-Containers.html
    ## systemctl cat container@wasabi.service
    #containers.wasabi = {
    #  ephemeral = true;
    #  autoStart = true;
    #  config = {
    #    config,
    #    pkgs,
    #    ...
    #  }: {
    #    services.httpd.enable = true;
    #    services.httpd.adminAddr = "foo@example.org";
    #    networking.firewall.allowedTCPPorts = [80];
    #    system.stateVersion = "22.11";
    #  };
    #};
  };
}
