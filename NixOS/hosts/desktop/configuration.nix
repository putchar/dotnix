# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
### IMPORTANT !!! CHECK THIS !!!
#https://nixos.wiki/wiki/Sway
{
  config,
  pkgs,
  lib,
  ...
}: {
  imports = [./hardware-configuration.nix];

  customNixOSModules = {
    nixUnstable.enable = true;
    pipewire.enable = true;
    docker.enable = true;
    sway.enable = true;
    openvpn.enable = true;
    media.enable = false;
    virt.enable = true;
    wireguard.enable = true;
    networkManager.enable = true;
  };

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  #boot.loader.systemd-boot.consoleMode = "max";
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.systemd-boot.configurationLimit = 10;
  boot.loader.systemd-boot.netbootxyz.enable = true;

  #nix.gc.options = "--delete-older-than 7d";
  # Define your hostname.
  networking.hostName = "desktop";

  networking.firewall.enable = true;
  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [24800];
  networking.firewall.allowedUDPPorts = [24800];
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.interfaces.enp6s0.useDHCP = true;

  services.xserver.videoDrivers = lib.mkDefault ["intel"];
  services.fwupd.enable = true;

  #nix.buildMachines = [{
  #  hostName = "dedibox-1";
  #  system = "x86_64-linux";
  #  # if the builder supports building for multiple architectures,
  #  # replace the previous line by, e.g.,
  #  # systems = ["x86_64-linux" "aarch64-linux"];
  #  maxJobs = 4;
  #  speedFactor = 2;
  #  supportedFeatures = [ "nixos-test" "benchmark" "big-parallel" "kvm" ];
  #  mandatoryFeatures = [ ];
  #}];
  #nix.distributedBuilds = true;
  ## optional, useful when the builder has a faster internet connection than yours
  #nix.extraOptions = ''
  #  builders-use-substitutes = true
  #'';
  programs.ssh.startAgent = true;

  programs.kdeconnect.enable = true;
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users = {
    putchar = {
      isNormalUser = true;
      shell = pkgs.bashInteractive;
      extraGroups = ["wheel"];
    };
  };
}
