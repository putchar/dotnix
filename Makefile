# https://github.com/NobbZ/nixos-config/blob/5f9d6bf0f5bace2026796afbb15ce37fa9ddc2a1/Makefile
# https://github.com/pinpox/nixos/blob/main/Makefile
# https://github.com/dguibert/dotfiles/blob/master/admin/nixops/flake.nix
# https://github.com/wiltaylor/dotfiles/blob/master/flake.nix
# https://github.com/serokell/deploy-rs
# https://github.com/hlissner/dotfiles
# https://github.com/mitchellh/nixos-config
# https://github.com/numtide/flake-utils
# https://github.com/johnrichardrinehart/JohnOS/tree/main
# https://github.com/NobbZ/nixos-config
# https://github.com/Misterio77/nix-starter-config
# https://dzone.com/users/1461657/mudrii.html
# https://teu5us.github.io/nix-lib.html
# https://gist.github.com/grahamc/4bdae755203b9fa75d310f1037c77ad5
# https://ilanjoselevich.com/blog/building-websites-using-nix-flakes-and-zola/
# https://mudrii.medium.com/
# https://github.com/KubqoA/dotfiles/blob/main/modules/desktop/swaywm.nix
# https://www.reddit.com/r/unixporn/comments/lepmss/sway_simple_sway_on_nixos/
# https://github.com/Gerschtli/nix-config/blob/master/flake.nix
# https://github.com/viperML/dotfiles/blob/54cedfff01163b1a10a49d46e8dcc5ed4aea90f8/templates/poetry-flake/flake.nix
# https://github.com/viperML/dotfiles/tree/master
# take a look at https://github.com/zhaofengli/colmena
# https://nixos.org/manual/nix/unstable/command-ref/new-cli/nix3-run.html#apps
# https://github.com/grahamc/nixos-config
# https://github.com/kamadorueda
# https://github.com/kamadorueda/machine
# https://dzone.com/articles/nixos-home-manager-on-native-nix-flake-installatio
# https://blog.ysndr.de/posts/guides/2021-12-01-nix-shells/
# https://github.com/AmeerTaweel/dotfiles/blob/master/flake.nix
# https://github.com/Mic92/dotfiles/blob/master/outputs.nix
HOSTNAME = $(shell hostname)
USER = $(shell whoami)
CPU = $(shell nproc)
HALF_CPU = $(shell expr ${CPU} / 2)

ifndef HOSTNAME
 $(error HOSTNAME unknown)
endif

ifndef USER
 $(error USER unknown)
endif

alejandra:
	alejandra .

# cf https://stackoverflow.com/questions/2214575/passing-arguments-to-make-run
deploy:
	deploy .#${REMOTE_HOST}

statix:
	nix-shell -p statix fd --command "fd -e nix -x statix check {}"

flake_update:
	nix flake update

full: home_manager_switch nixos_switch

garbage_collect:
	cd /nix/var/nix/profiles && (ls -1dv system-*-link | head -n -4 | xargs --no-run-if-empty sudo rm) && sudo nix-collect-garbage
#	/run/current-system/sw/bin/find /nix/var/nix/profiles/ -name 'system-*-link' -print0 | sort -rnz | tail -zn+5 | xargs -r0 -n1 printf "sudo rm %s\n"
	nix-collect-garbage -d
#sudo nix-collect-garbage -d

home_manager_diff:
	nix profile diff-closures --profile /nix/var/nix/profiles/per-user/${USER}/home-manager

home_manager_closure_size:
	nix build .#homeConfigurations.${USER}@${HOSTNAME}.activationPackage
	nix path-info -hS .#homeConfigurations.${USER}@${HOSTNAME}.activationPackage

home_manager_build:
	home-manager build --cores ${HALF_CPU} --flake .#${USER}@${HOSTNAME} -v

home_manager_switch:
## other method
## nix build --profile /home/${USER}/.local/state/nix/profiles/home-manager   .#homeConfigurations.${USER}@${HOSTNAME}.activationPackage
	home-manager switch --cores ${HALF_CPU} --flake .#${USER}@${HOSTNAME} -v
	echo "### HOME-MANAGER DIFF ###"
	ls -v -d /home/${USER}/.local/state/nix/profiles/home-manager-* | tail -n 2 | xargs nvd diff
	echo ""

nixos_diff:
	nix profile diff-closures --profile /nix/var/nix/profiles/system

nixos_closure_size:
	nix build .#nixosConfigurations.${HOSTNAME}.config.system.build.toplevel
	nix path-info -hS .#nixosConfigurations.${HOSTNAME}.config.system.build.toplevel

nixos_build:
## other method
## nix build .#nixosConfigurations.${HOSTNAME}.config.system.build.toplevel
	nixos-rebuild build --cores ${HALF_CPU} --flake .#${HOSTNAME} -L
	nvd diff $$(ls -v --reverse /nix/var/nix/profiles | head --lines=1 | awk '{print "/nix/var/nix/profiles/" $$0}' -) result

nixos_boot:
	nixos-rebuild boot --cores ${HALF_CPU} --flake .#${HOSTNAME} -L -v --use-remote-sudo
	echo "### NIXOS DIFF ###"
	ls -v /nix/var/nix/profiles | tail -n 2 | awk '{print "/nix/var/nix/profiles/" $$0}' - | xargs nvd diff

nixos_switch:
## other method
## sudo nix build --profile /nix/var/nix/profiles/system  .#nixosConfigurations.${HOSTNAME}.config.system.build.toplevel
## CF https://github.com/NixOS/nixpkgs/issues/169193
#	sudo nixos-rebuild switch --flake .#${HOSTNAME} -L -v
	nixos-rebuild switch --cores ${HALF_CPU} --flake .#${HOSTNAME} -L -v --use-remote-sudo
	echo "### NIXOS DIFF ###"
	ls -v /nix/var/nix/profiles | tail -n 2 | awk '{print "/nix/var/nix/profiles/" $$0}' - | xargs nvd diff
	echo ""

nixos_vm:
	nixos-rebuild build-vm --cores ${HALF_CPU} --flake .#${HOSTNAME} -L

vscode_update:
	bash ./Home-Manager/modules/vscode/generate_extension_file.sh
	alejandra ./Home-Manager/modules/vscode/extensionsList.nix
