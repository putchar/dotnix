#!/usr/bin/env sh
## DRY RUN
#sudo nixos-rebuild dry-build -I nixos-config=./system/configuration.nix
#./update-system.sh && ./apply-system.sh

#[root@nixos-desktop:~]# nix-channel --list
#nixos https://nixos.org/channels/nixos-unstable
#nixos-hardware https://github.com/NixOS/nixos-hardware/archive/master.tar.gz
#nixpkgs https://github.com/NixOS/nixpkgs/archive/master.tar.gz
# one day : nix-channel add https://github.com/nixos/nixpkgs/archive/master.tar.gz master  or something like that

GITHUB_NIXPKGS_REPO="/home/$USER/Documents/github.com/NixOS/nixpkgs"
NIXOS_CHANNEL_UPDATE="sudo nix-channel --update"
NIXOS_REBUILD_CMD="sudo nixos-rebuild switch --cores 2 --upgrade -I nixos-config=./NixOS/Systems/$HOSTNAME/configuration.nix"
#LOCAL_BUILD="-I nixpkgs=$GITHUB_NIXPKGS_REPO"
DIFF_CMD="ls /nix/var/nix/profiles | tail -n 2 | awk '{print \"/nix/var/nix/profiles/\" \$0}' - | xargs nvd diff"

pushd ~/Documents/gitlab.com/putchar/dotnix
if [[ -f "./NixOS/Systems/$HOSTNAME/configuration.nix" ]]; then
    echo "##"
    echo "## $NIXOS_CHANNEL_UPDATE"
    echo "##"
    eval $NIXOS_CHANNEL_UPDATE
    echo ""

    echo "##"
    echo "## $NIXOS_REBUILD_CMD"
    echo "##"
    eval $NIXOS_REBUILD_CMD $LOCAL_BUILD
    echo ""

    echo "##"
    echo "## $DIFF_CMD"
    echo "##"
    eval $DIFF_CMD
    echo ""
else
    echo "file: ~/Documents/gitlab.com/putchar/dotnix/NixOS/Systems/$HOSTNAME/configuration.nix not found"
fi
popd
