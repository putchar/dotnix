IS_UEFI=0
if [[ -d /sys/firmware/efi ]]; then
    echo "UEFI compatible system"
    IS_UEFI=1
else
    echo "LEGACY compatible system"
fi

DEVICE="/dev/sda"
BOOT_PARTITION="${DEVICE}1"
ROOT_PARTITION="${DEVICE}2"
BOOT_LABEL="NIXBOOT"
ROOT_LABEL="NIXROOT"
MNT_DIR="/mnt"
CRYPTROOT="cryptroot"

parted_dos() {
    parted --script "${DEVICE}" -- mklabel msdos
    parted --script "${DEVICE}" -- mkpart primary 1MiB 512MiB
    parted --script "${DEVICE}" -- set 1 boot on
    parted --script "${DEVICE}" -- mkpart primary 512MiB 100%
    parted --script "${DEVICE}" -- print
}

parted_uefi() {
    parted --script "${DEVICE}" -- mklabel gpt
    parted --script "${DEVICE}" -- mkpart ESP fat32 1MiB 512MiB
    parted --script "${DEVICE}" -- set 1 esp on
    parted --script "${DEVICE}" -- mkpart primary 512MiB 100%
    parted --script "${DEVICE}" -- print
}

crypt_root_format() {
    cryptsetup luksFormat $ROOT_PARTITION
    cryptsetup luksOpen $ROOT_PARTITION $CRYPTROOT
    mkfs.fat -F 32 -n $BOOT_LABEL $BOOT_PARTITION
    mkfs.ext4 -L $ROOT_LABEL /dev/mapper/$CRYPTROOT
}

format() {
    mkfs.fat -F 32 -n $BOOT_LABEL $BOOT_PARTITION
    mkfs.ext4 -L $ROOT_LABEL $ROOT_PARTITION
}

setup_mount() {
    mount /dev/disk/by-label/$ROOT_LABEL $MNT_DIR/
    mkdir $MNT_DIR/boot
    mount /dev/disk/by-label/$BOOT_LABEL $MNT_DIR/boot/
}

generate_config() {
    sudo nixos-generate-config --root $MNT_DIR
}

install_nixos() {
    sudo nixos-install --root $MNT_DIR
}


# uefi normal install (no cryptroot)
# parted /dev/vda -- mklabel gpt
# parted /dev/vda -- mkpart ESP fat32 1MiB 512MiB
# parted /dev/vda -- set 1 esp on
# parted /dev/vda -- mkpart primary 512MiB 100%
# mkfs.fat -F 32 -n NIXBOOT /dev/vda1
# mkfs.ext4 -L NIXROOT /dev/vda2
# mount /dev/disk/by-label/NIXROOT /mnt/
# mkdir /mnt/boot
# mount /dev/disk/by-label/NIXBOOT /mnt/boot/
# sudo nixos-generate-config --root /mnt
# sudo nixos-install --root /mnt

## uefi cryptroot (no lvm)
# parted /dev/vda -- mklabel gpt
# parted /dev/vda -- mkpart ESP fat32 1MiB 512MiB
# parted /dev/vda -- set 1 esp on
# parted /dev/vda -- mkpart primary 512MiB 100%

# it will ask for a passphrase
# cryptsetup luksFormat --label=CRYPTROOT /dev/vda2

# it will ask for a passphrase
# cryptsetup luksOpen /dev/vda2 cryptroot

# mkfs.fat -F 32 -n NIXBOOT /dev/vda1
# mkfs.ext4 -L NIXROOT /dev/mapper/cryptroot
# mount /dev/disk/by-label/NIXROOT /mnt/
# mkdir /mnt/boot
# mount /dev/disk/by-label/NIXBOOT /mnt/boot/
# sudo nixos-generate-config --root /mnt

# vous editez votre conf blabla blabla
# cat /mnt/etc/nixos/configuration.nix
# cat /mnt/etc/nixos/hardware-configuration.nix
# sudo nixos-install --root /mnt


#[root@nixos:/dev/disk/by-label]# history

#    3  parted /dev/nvme0n1 -- mklabel gpt
#    5  parted /dev/nvme0n1 -- mkpart ESP fat32 1Mib 512Mib
#    6  parted /dev/nvme0n1 -- set 1 esp on
#    7  parted /dev/nvme0n1 -- mkpart primary 512Mib 100%
#   11  cryptsetup luksFormat --label=CRYPTROOT  /dev/nvme0n1p2
#   12  cryptsetup luksOpen /dev/nvme0n1p2 cryptroot
#   14  mkfs.fat -F 32 -n NIXBOOT /dev/nvme0n1p1
#   15  mkfs.ext4 -L NIXROOT /dev/mapper/cryptroot
#   16  mount /dev/disk/by-label/NIXROOT /mnt/
#   17  mkdir /mnt/boot
#   18  mount /dev/disk/by-label/NIXBOOT /mnt/boot/
#   19  sudo nixos-generate-config --root /mnt

#   20  cd /mnt/etc/
#   21  ls
#   22  cd nixos/
#   23  ls
#   24  vim hardware-configuration.nix
#   25  cd /dev/disk/by-label/
#   26  ls
#   27  vim /mnt/etc/nixos/hardware-configuration.nix
#   28  vim /mnt/etc/nixos/configuration.nix
#   29  vim /mnt/etc/nixos/hardware-configuration.nix
#   30  vim /mnt/etc/nixos/configuration.nix
#   31  history
# sudo dd if=/dev/zero of=/swapFile bs=1M count=32768
# chmod 600 /swapFile
# mkswap /swapFile
# swapon /swapFile
