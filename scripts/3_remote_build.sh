#!/usr/bin/env sh
## DRY RUN
#sudo nixos-rebuild dry-build -I nixos-config=./system/configuration.nix
#./update-system.sh && ./apply-system.sh

#[root@nixos-desktop:~]# nix-channel --list
#nixos https://nixos.org/channels/nixos-unstable
#nixos-hardware https://github.com/NixOS/nixos-hardware/archive/master.tar.gz
#nixpkgs https://github.com/NixOS/nixpkgs/archive/master.tar.gz
# one day : nix-channel add https://github.com/nixos/nixpkgs/archive/master.tar.gz master  or something like that

pushd ~/Documents/gitlab.com/putchar/dotnix
if [[ -f "./NixOS/Systems/$HOSTNAME/configuration.nix" ]]; then
    echo "##"
    echo "## sudo nix-channel --update"
    echo "##"
    sudo nix-channel --update
    echo ""

    echo "##"
    echo "## sudo nixos-rebuild switch --upgrade -I nixos-config=./NixOS/Systems/$HOSTNAME/configuration.nix -j0"
    echo "##"
    sudo nixos-rebuild switch --upgrade -I nixos-config=./NixOS/Systems/$HOSTNAME/configuration.nix -j0
    echo ""

    echo "#### DIFF ####"
    echo ""
    ls "/nix/var/nix/profiles" | tail -n 2 | awk '{print "/nix/var/nix/profiles/" $0}' - | xargs nvd diff
    echo ""
else
    echo "file: ~/Documents/gitlab.com/putchar/dotnix/NixOS/Systems/$HOSTNAME/configuration.nix not found"
fi
popd

pushd ~/Documents/gitlab.com/putchar/dotnix
if [[ -f "./Home-Manager/Users/$HOSTNAME/$USER/home.nix" ]]; then
    echo "##"
    echo "## nix-channel --update"
    echo "##"
    nix-channel --update
    echo ""

    echo "##"
    echo "## home-manager -j 0 switch -f ./Home-Manager/Users/$HOSTNAME/$USER/home.nix"
    echo "##"
    home-manager -j 0 switch -f ./Home-Manager/Users/$HOSTNAME/$USER/home.nix
    echo ""

    echo "#### DIFF ####"
    echo ""
    ls -d /nix/var/nix/profiles/per-user/$USER/home* | tail -n 2 | xargs nvd diff
    echo ""
else
    "file: ~/Documents/gitlab.com/putchar/dotnix/Home-Manager/Users/$HOSTNAME/$USER/home.nix not found, exiting"
fi
