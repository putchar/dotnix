#!/usr/bin/env sh

if [ -z "$1" ]
then
    echo "No argument supplied"
    exit 1
fi
MACHINE="$1"


check_link() {
    nc -w 1 -vz $MACHINE 22
}

send_data() {
    pushd ~/Documents/gitlab.com/putchar/dotnix
    ssh $USER@$MACHINE rm -rf ~/Documents/gitlab.com/putchar/dotnix
    ssh $USER@$MACHINE mkdir -p ~/Documents/gitlab.com/putchar/dotnix
    rsync -azvr . $USER@$MACHINE:~/Documents/gitlab.com/putchar/dotnix
    popd
}

if check_link
then
    send_data
else
    echo "##################################"
    echo "## Cannot send data to $MACHINE ##"
    echo "##################################"
    exit 1
fi

echo "###########################"
echo "## Data sent to $MACHINE ##"
echo "###########################"
exit 0
