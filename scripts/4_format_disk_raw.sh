# uefi normal install (no CRYPTROOT)
parted -s /dev/vda -- mklabel gpt
parted -s /dev/vda -- mkpart ESP fat32 1MiB 512MiB
parted -s /dev/vda -- set 1 esp on
parted -s /dev/vda -- mkpart primary 512MiB 100%
mkfs.fat -F 32 -n NIXBOOT /dev/vda1
mkfs.ext4 -F -L NIXROOT /dev/vda2
mount /dev/disk/by-label/NIXROOT /mnt/
mkdir /mnt/boot
mount /dev/disk/by-label/NIXBOOT /mnt/boot/
sudo nixos-generate-config --root /mnt
sudo nixos-install --root /mnt

# uefi CRYPTROOT (no lvm)
parted /dev/vda -- mklabel gpt
parted /dev/vda -- mkpart ESP fat32 1MiB 512MiB
parted /dev/vda -- set 1 esp on
parted /dev/vda -- mkpart primary 512MiB 100%

# it will ask for a passphrase
cryptsetup luksFormat --label=CRYPTROOT /dev/vda2

# it will ask for a passphrase
cryptsetup luksOpen /dev/vda2 CRYPTROOT

mkfs.fat -F 32 -n NIXBOOT /dev/vda1
mkfs.ext4 -L NIXROOT /dev/mapper/CRYPTROOT
mount /dev/disk/by-label/NIXROOT /mnt/
mkdir /mnt/boot
mount /dev/disk/by-label/NIXBOOT /mnt/boot/
sudo nixos-generate-config --root /mnt

# vous editez votre conf blabla blabla
# cat /mnt/etc/nixos/configuration.nix
# cat /mnt/etc/nixos/hardware-configuration.nix
sudo nixos-install --root /mnt
# we now add user password
sudo nixos-enter





#[root@nixos:/dev/disk/by-label]# history

parted /dev/nvme0n1 -- mklabel gpt
parted /dev/nvme0n1 -- mkpart ESP fat32 1Mib 512Mib
parted /dev/nvme0n1 -- set 1 esp on
parted /dev/nvme0n1 -- mkpart primary 512Mib 100%

cryptsetup luksFormat --label=CRYPTROOT  /dev/nvme0n1p2

cryptsetup luksOpen /dev/nvme0n1p2 CRYPTROOT

mkfs.fat -F 32 -n NIXBOOT /dev/nvme0n1p1

mkfs.ext4 -L NIXROOT /dev/mapper/CRYPTROOT

mount /dev/disk/by-label/NIXROOT /mnt/

mkdir /mnt/boot

mount /dev/disk/by-label/NIXBOOT /mnt/boot/

# sudo dd if=/dev/zero of=/swapFile bs=1M count=32768
# chmod 600 /swapFile
# mkswap /swapFile
# swapon /swapFile

sudo nixos-generate-config --root /mnt
sudo nixos-install --root /mnt
sudo nixos-enter
