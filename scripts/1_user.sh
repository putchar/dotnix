#!/usr/bin/env sh

#./update-users.sh && ./apply-users.sh
# nix-channel --add https://github.com/nix-community/home-manager/archive/release-20.09.tar.gz home-manager
# nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager
# nix-channel --update
# nix-shell '<home-manager>' -A install

HM_CHANNEL_UPDATE="nix-channel --update"
HM_REBUILD_CMD="home-manager -j 2 switch -f ./Home-Manager/Users/$HOSTNAME/$USER/home.nix"
DIFF_CMD="ls -d /nix/var/nix/profiles/per-user/$USER/home* | tail -n 2 | xargs nvd diff"

pushd ~/Documents/gitlab.com/putchar/dotnix
if [[ -f "./Home-Manager/Users/$HOSTNAME/$USER/home.nix" ]]; then
    echo "##"
    echo "## $HM_CHANNEL_UPDATE"
    echo "##"
    eval $HM_CHANNEL_UPDATE
    echo ""

    echo "##"
    echo "##  $HM_REBUILD_CMD"
    echo "##"
    eval $HM_REBUILD_CMD
    echo ""

    echo "##"
    echo "##  $DIFF_CMD"
    echo "##"
    eval $DIFF_CMD
    echo ""
else
    echo "file: ~/Documents/gitlab.com/putchar/dotnix/Home-Manager/Users/$HOSTNAME/$USER/home.nix not found, exiting"
fi
popd
