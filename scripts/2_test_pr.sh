#!/usr/bin/env sh

set -o errexit
set -o nounset
set -o pipefail

# make sure you have a working pkgs.gh with configured credentials

# github nixos organisation location
GITHUB_NIXOS_DIR="/home/$USER/Documents/github.com/NixOS"
# make sure you have nixos/nixpkgs repo somewhere
GITHUB_NIXPKGS_REPO="/home/$USER/Documents/github.com/NixOS/nixpkgs"
# custom nixos configuration location
NIXOS_CONFIG="/home/$USER/Documents/gitlab.com/$USER/dotnix"

if [ $# -eq 0 ]; then
    echo "No arguments supplied"
    exit 1
fi

if [[ -d "$GITHUB_NIXPKGS_REPO" ]]; then
    cd $GITHUB_NIXPKGS_REPO
    git checkout master
    git pull
    gh pr checkout $1
else
    mkdir -p $GITHUB_NIXOS_DIR
    cd $GITHUB_NIXOS_DIR
    git clone git@github.com:NixOS/nixpkgs.git
    cd $GITHUB_NIXPKGS_REPO
    gh pr checkout $1
fi

### REVIEW PHASE
nixpkgs-review pr $1
exit

cd $NIXOS_CONFIG
if [[ -f "./NixOS/Systems/$HOSTNAME/configuration.nix" ]]; then
    echo "##"
    echo "## sudo nixos-rebuild switch --upgrade \
    -I nixpkgs=$GITHUB_NIXPKGS_REPO \
    -I nixos-config=./NixOS/Systems/$HOSTNAME/configuration.nix -j0"
    echo "##"
    sudo nixos-rebuild switch --upgrade \
    -I nixpkgs=$GITHUB_NIXPKGS_REPO \
    -I nixos-config=./NixOS/Systems/$HOSTNAME/configuration.nix -j0
    echo ""

    echo "#### DIFF ####"
    echo ""
    ls "/nix/var/nix/profiles" | tail -n 2 | awk '{print "/nix/var/nix/profiles/" $0}' - | xargs nvd diff
    echo ""
else
    echo "file: $NIXOS_CONFIG/NixOS/Systems/$HOSTNAME/configuration.nix not found"
fi
