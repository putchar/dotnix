inputs: {
  mkNixosSystem = import ./mkNixosSystem.nix inputs;
  mkHomeManagerConfiguration = import ./mkHomeManagerConfiguration.nix inputs;
}
