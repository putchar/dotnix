inputs @ {self, ...}: {
  system ? "x86_64-linux",
  username ? "putchar",
  hostname,
  stateVersion ? "22.05",
  extraModules ? [],
}:
inputs.home-manager.lib.homeManagerConfiguration {
  ## cf https://github.com/nix-community/home-manager/issues/2942#issuecomment-1119760100
  pkgs = import inputs.nixos-unstable {
    system = system;
    #config.allowUnfree = true;
  };

  #extraSpecialArgs     = {inherit inputs;};
  modules =
    [
      {_module.args = {inherit inputs;};}
      "${self}/Home-Manager/modules"
      "${self}/Home-Manager/hosts/${hostname}/users/${username}/home.nix"
      {home.stateVersion = stateVersion;}
    ]
    ++ extraModules;
}
