inputs @ {self, ...}: {
  system ? "x86_64-linux",
  hostname,
  extraModules ? [],
}:
inputs.nixos-unstable.lib.nixosSystem {
  system = system;
  #specialArgs = {inherit inputs;};
  modules =
    [
      {
        _module.args = {
          inherit inputs;
        };
      }
      "${self}/NixOS/modules"
      "${self}/NixOS/hosts/${hostname}/configuration.nix"
    ]
    ++ extraModules;
}
