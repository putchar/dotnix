final: prev: let
  patchedPkgs =
    import (
      builtins.fetchTarball {
        url = "https://github.com/nixos/nixpkgs/archive/ffdadd3ef9167657657d60daf3fe0f1b3176402d.tar.gz";
        sha256 = "1nrz4vzjsf3n8wlnxskgcgcvpwaymrlff690f5njm4nl0rv22hkh";
      }
    ) {
      inherit (prev) system config;
      # inherit (prev) overlays;  # not sure
    };
  patchedPam = patchedPkgs.pam;
in {
  i3lock = prev.i3lock.override {pam = patchedPam;};
  #betterlockscreen = patchedPkgs.betterlockscreen.override {
  # i3lock-color = prev.i3lock-color.override { pam = patchedPam; };
  #};
  i3lock-color = prev.i3lock-color.override {pam = patchedPam;};
  swaylock-effects = prev.swaylock-effects.override {pam = patchedPam;};
  swaylock = prev.swaylock.override {pam = patchedPam;};
  #swaylock-fancy  = prev.swaylock-fancy.override { pam = patchedPam; };
  # apply the same patch to other packages
}
