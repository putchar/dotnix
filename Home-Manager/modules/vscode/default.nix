{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customHomeManagerModules.vscode;
  jsonFormat = pkgs.formats.json {};
in {
  options.customHomeManagerModules.vscode = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable vscode globally or not
      '';
    };

    userSettings = lib.mkOption {
      type = jsonFormat.type;
      default = {};
    };

    extraUserSettings = lib.mkOption {
      type = jsonFormat.type;
      default = {};
    };
  };

  config = lib.mkIf cfg.enable {
    home.packages = [
      pkgs.exercism
    ];

    programs.go = {
      enable = true;
      goPath = "Documents/go";
      goBin = "Documents/go/bin";
    };
    #home.sessionVariables.GOPATH = programs.go.goPath;

    #programs.bash.shellAliases = {
    #  code = "NIXOS_OZONE_WL=1 ${pkgs.vscode}/bin/code";
    #};

    #xdg.desktopEntries.code = {
    #  name = "Visual Studio Code";
    #  genericName = "Text Editor (Wayland)";
    #  exec = "env NIXOS_OZONE_WL=1 ${pkgs.vscode}/bin/code %F";
    #  icon = "code";
    #  #desktopName = "Visual Studio Code";
    #  comment = "Code Editing. Redefined.";
    #  type = "Application";
    #  startupNotify = true;
    #  #startupWMClass = "Code";
    #  categories = ["Utility" "TextEditor" "Development" "IDE"];
    #  mimeType = ["text/plain" "inode/directory"];
    #  #keywords = ["vscode"];
    #  actions.new-empty-window = {
    #    name = "New Empty Window";
    #    exec = "env NIXOS_OZONE_WL=1 ${pkgs.vscode}/bin/code --new-window %F";
    #    icon = "code";
    #  };
    #};

    programs.vscode = {
      enable = cfg.enable;

      extensions =
        import ./mkAllExtensions.nix {inherit pkgs;};

      #  import ./mkExtensions.nix {inherit pkgs;};
      mutableExtensionsDir = false;
      keybindings = [
        {
          key = "ctrl+up";
          command = "cursorColumnSelectUp";
        }
        {
          key = "ctrl+down";
          command = "cursorColumnSelectDown";
        }
        {
          key = "ctrl+escape";
          command = "workbench.action.terminal.toggleTerminal";
        }
        {
          key = "ctrl+`";
          command = "workbench.action.terminal.toggleTerminal";
        }
        {
          key = "ctrl+alt+=";
          command = "wwm.aligncode";
          when = "editorTextFocus && !editorReadonly";
        }
        {
          key = "ctrl+shift+i";
          command = "editor.action.formatDocument";
          when = "editorHasDocumentFormattingProvider && editorTextFocus && !editorReadonly && !inCompositeEditor";
        }
        {
          key = "ctrl+alt+s";
          command = "extension.multiCommand.execute";
          args = {
            sequence = [
              "editor.action.selectAll"
              "editor.action.formatDocument"
              "wwm.aligncode"
              "workbench.action.files.save"
            ];
          };
          when = "editorHasDocumentFormattingProvider && editorTextFocus && !editorReadonly && !inCompositeEditor";
        }
      ];
      userSettings =
        {
          #"ansible.ansible.path"                              = "${pkgs.ansible}/bin/ansible";
          "ansible.ansible.useFullyQualifiedCollectionNames" = true;
          "ansible.ansibleLint.enabled" = true;
          #          "ansible.ansibleLint.path" = "${pkgs.ansible-lint}/bin/ansible-lint";
          #          "ansible.ansibleLint.path" = "${pkgs.python39Packages.ansible-lint}/bin/ansible-lint";
          "ansible.python.interpreterPath" = "${pkgs.python3}/bin/python3";
          "python.analysis.completeFunctionParens" = true;
          "python.autoComplete.addBrackets" = true;
          "python.formatting.provider" = "black";
          "python.formatting.blackPath" = "${pkgs.python3Packages.black}/bin/black";
          "python.linting.pylintEnabled" = true;
          "python.linting.pylintPath" = "${pkgs.python3Packages.pylint}/bin/pylint";
          "python.linting.enabled" = true;
          "python.languageServer" = "Pylance";

          "github.copilot.inlineSuggest.count" = 3;
          "github.copilot.list.count" = 10;
          "github.copilot.autocomplete.count" = 3;
          "github.copilot.autocomplete.enable" = true;
          "github.copilot.inlineSuggest.enable" = true;
          "github.copilot.enable" = {
            "python" = true;
            "ansible" = true;
          };

          "extensions.autoUpdate" = false;
          "extensions.autoCheckUpdates" = false;
          "editor.fontFamily" = "'Hack Nerd Font', 'Ubuntu Mono', 'Cascadia Mono', 'DejaVu Sans Mono', 'Font Awesome 5 Brands', 'Font Awesome 5 Free', 'Font Awesome 5 Free Solid'";
          "editor.fontLigatures" = true;
          "editor.fontSize" = 11;
          "editor.fontWeight" = "bold";
          "editor.formatOnSave" = true;
          "editor.renderWhitespace" = "all";
          "files.insertFinalNewline" = true;
          "files.trimFinalNewlines" = true;
          "files.trimTrailingWhitespace" = true;
          "trailing-spaces.trimOnSave" = true;
          "highlightLine.borderColor" = "#abb2bf";
          "highlightLine.borderStyle" = "solid";
          "highlightLine.borderWidth" = "1px";
          "terminal.integrated.profiles.linux" = {
            "bash" = {
              "path" = "${pkgs.bashInteractive}/bin/bash";
              "icon" = "terminal-bash";
            };
            "tmux" = {
              "path" = "${pkgs.tmux}/bin/tmux";
              "icon" = "terminal-tmux";
            };
          };
          "terminal.integrated.defaultProfile.linux" = "bash";
          "terminal.external.linuxExec" = "alacritty";
          "terminal.integrated.fontFamily" = "'Hack Nerd Font', 'Ubuntu Mono', 'Cascadia Mono', 'DejaVu Sans Mono', 'Font Awesome 5 Brands', 'Font Awesome 5 Free', 'Font Awesome 5 Free Solid'";
          "terminal.integrated.fontSize" = 12;
          "terminal.integrated.fontWeight" = "bold";

          "window.zoomLevel" = 1;
          "explorer.confirmDelete" = false;
          "explorer.confirmDragAndDrop" = false;
          "explorer.openEditors.visible" = 0;
          "editor.occurrencesHighlight" = "singleFile";
          "editor.rulers" = [
            {
              "column" = 80;
              "color" = "#ff9900";
            }
            {
              "column" = 120;
              "color" = "#ff4081";
            }
          ];
          "workbench.iconTheme" = "material-icon-theme";
          #"workbench.iconTheme" = "vscode-icons";
          #"workbench.iconTheme"   = "ayu";
          "workbench.colorTheme" = "Atom One Dark";
          #"workbench.colorTheme" = "Moonlight II";

          ## bracket color stuff
          "editor.bracketPairColorization.enabled" = true;
          "editor.guides.bracketPairs" = false;
          "editor.guides.bracketPairsHorizontal" = true;
          "editor.guides.highlightActiveBracketPair" = true;
          "workbench.colorCustomizations" = {
            "editorRuler.foreground" = "#ff4081";
            "editorBracketHighlight.foreground1" = "#ffd700";
            "editorBracketHighlight.foreground2" = "#ff6600";
            "editorBracketHighlight.foreground3" = "#ff3232";
            "editorBracketHighlight.foreground4" = "#c934c4";
            "editorBracketHighlight.foreground5" = "#5680f3";
            "editorBracketHighlight.foreground6" = "#1db954";
            "editorBracketHighlight.unexpectedBracket.foreground" = "#ff0000";
          };
          "files.associations" = {
            "config" = "properties";
            "i3_config" = "properties";
            "dunstrc" = "properties";
            "Dockerfile*" = "dockerfile";
            "*.dockerfile" = "dockerfile";
            "docker-compose.yml" = "dockercompose";
            "*.docker-compose.yml" = "dockercompose";
            "alerts.rules" = "jinja-yaml";
            "Pipfile" = "pip-requirements";
            "*.rasi" = "css";
            "jenkinsfile" = "groovy";
            "Jenkinsfile" = "groovy";
            "*.groovy" = "groovy";
            "*.groovy.j2" = "jinja-groovy";
            "*.j2" = "jinja";
            "*.yml" = "ansible";
            "*.xml.j2" = "jinja-xml";
            "*.yml.j2" = "jinja-yaml";
            "*.yaml.j2" = "jinja-yaml";
            "*.conf.j2" = "jinja-properties";
            "*.tf" = "terraform";
            "flake.lock" = "json";
            "*.boot" = "clojure";
            "*.boot.j2" = "clojure";
            "*.clj" = "clojure";
            "*.clj.j2" = "clojure";
            "*.properties.j2" = "jinja-properties";
            "inventory*" = "jinja-properties";
            "*.env*" = "dotenv";
            "*Dockerfile.j2" = "jinja-dockerfile";
            "*.yuck" = "yuck";
            "*.sh" = "shellscript";

            #"*.nix"                 = "nix";
            #"Makefile"              = "Makefile";
            #"**/*.yaml"             = "kubernetes";
            #"**/hosts.yml"          = "ansible";
            #"**/inventory.yml"      = "ansible";
            #"**/defaults/**/*"      = "ansible";
            #"**/tasks/**/*.yml"     = "ansible";
            #"**/handler/*.yml"      = "ansible";
            #"**/*_vars/**/*.yml"    = "ansible";
            #"**/roles/**/*.yml"     = "ansible";
            #"**/playbooks/**/*.yml" = "ansible";
            #"**/*ansible*/**/*.yml" = "ansible";
            #"**/vars/**/*.yml"      = "ansible";
            #"**/inventory/*/*"      = "ansible";
            #"**/templates/**.j2"    = "ansible";
          };
          "security.workspace.trust.untrustedFiles" = "open";
          "files.exclude" = {"**/.git" = false;};
          "settingsSync.keybindingsPerPlatform" = false;
          #"yaml.format.enable"                   = true;
          #"yaml.format.singleQuote"              = false;
          #"yaml.format.bracketSpacing"           = true;
          #"yaml.validate"                        = true;
          #"yaml.completion"                      = true;
          #"yaml.schemas"                         = {
          # "kubernetes"                                          = "**/*.yaml";
          # "https://json.schemastore.org/ansible-inventory.json" = [
          #    "**/hosts.yml"
          #    "**/inventory.yml"
          #    "**/molecule/*.yml"
          #  ];
          #};
          "nix.enableLanguageServer" = false;
          "shellformat.path" = "${pkgs.shfmt}/bin/shfmt";

          #"yaml.schemaStore.enable"   = true;
          "[html]" = {
            "editor.defaultFormatter" = "vscode.html-language-features";
          };
          "[json]" = {
            "editor.defaultFormatter" = "vscode.json-language-features";
            "editor.tabSize" = 2;
            "editor.insertSpaces" = true;
            "editor.autoIndent" = "full";
            "editor.formatOnSave" = true;
          };
          "[jsonc]" = {
            "editor.defaultFormatter" = "vscode.json-language-features";
            "editor.tabSize" = 2;
            "editor.insertSpaces" = true;
            "editor.autoIndent" = "full";
            "editor.formatOnSave" = true;
          };
          "[markdown]" = {"editor.formatOnSave" = false;};
          "markdown.marp.toggleMarpFeature" = true;
          "[nix]" = {
            "editor.insertSpaces" = true;
            "editor.tabSize" = 2;
            "editor.autoIndent" = "full";
            "editor.quickSuggestions" = {
              "other" = true;
              "comments" = false;
              "strings" = true;
            };
            "editor.formatOnSave" = true;
            "editor.formatOnPaste" = false;
            "editor.formatOnType" = false;
          };
          "alejandra.program" = "${pkgs.alejandra}/bin/alejandra";
          "[terraform]" = {
            "editor.insertSpaces" = true;
            "editor.tabSize" = 2;
            "editor.autoIndent" = "full";
            "editor.quickSuggestions" = {
              "other" = true;
              "comments" = false;
              "strings" = true;
            };
            "editor.formatOnSave" = true;
          };
          "[shellscript]" = {
            "editor.defaultFormatter" = "foxundermoon.shell-format";
          };
          #"[Makefile]" = {
          # "editor.insertSpaces"     = true;
          # "editor.tabSize"          = 2;
          # "editor.autoIndent"       = "full";
          # "editor.quickSuggestions" = {
          # "other"    = true;
          # "comments" = false;
          # "strings"  = true;
          #  };
          # "editor.formatOnSave" = true;
          #};
          "[yaml]" = {
            "editor.insertSpaces" = true;
            "editor.tabSize" = 2;
            "editor.autoIndent" = "full";
            "editor.quickSuggestions" = {
              "other" = true;
              "comments" = false;
              "strings" = true;
            };
            "editor.formatOnSave" = true;
            "editor.formatOnPaste" = false;
          };
          "search.useGlobalIgnoreFiles" = true;
          "git.confirmSync" = false;
          #"redhat.telemetry.enabled" = false;
          "update.mode" = "none";
          #"window.titleBarStyle" = "custom";
          "vsicons.dontShowNewVersionMessage" = true;
        }
        // cfg.extraUserSettings;
    };
  };
}
