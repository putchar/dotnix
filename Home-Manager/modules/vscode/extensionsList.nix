{
  extensions = [
    {
      name = "vscode-theme-onedark";
      publisher = "akamud";
      version = "2.3.0";
      sha256 = "1km3hznw8k0jk9sp3r81c89fxa311lc6gw20fqikd899pvhayqgh";
    }
    {
      name = "nix-env-selector";
      publisher = "arrterian";
      version = "1.0.11";
      sha256 = "113zx78c3219knw4qa04242404n32vnk9rb6a3ynz41dgwh1mbbl";
    }
    {
      name = "moonlight";
      publisher = "atomiks";
      version = "0.11.1";
      sha256 = "0klbgjwx9hvjlri604j6i9scj005wbw31h7dxw5zzrnnlcxx2wb1";
    }
    {
      name = "vscode-better-align";
      publisher = "chouzz";
      version = "1.4.2";
      sha256 = "0fb3d1j4qr90jigjxarpnnq1lx7ykzdgzlmg7zmcvia2180h6zf3";
    }
    {
      name = "highlight-line-vscode";
      publisher = "cliffordfajardo";
      version = "0.0.1";
      sha256 = "0lrj482ks65gnr9ncn6kzzya9iw4pg2fng9hasp416z0qv7gxjfg";
    }
    {
      name = "vscode-zonefile";
      publisher = "compilenix";
      version = "0.0.4";
      sha256 = "18659gma90wnnmzr1zmsfihp1vzn5siyyxp1w5fm8ax9c7bd261m";
    }
    {
      name = "vscode-eslint";
      publisher = "dbaeumer";
      version = "3.0.5";
      sha256 = "1cmkgi1i5c7qkrr8cif36i803yl6mrv87y9gmzfb701pcfg8yxx9";
    }
    {
      name = "prettier-vscode";
      publisher = "esbenp";
      version = "10.4.0";
      sha256 = "1iy7i0yxnhizz40llnc1dk9q8kk98rz6ki830sq7zj3ak9qp9vzk";
    }
    {
      name = "yuck";
      publisher = "eww-yuck";
      version = "0.0.3";
      sha256 = "1hxdxa13s1vlilw7fidr8vnl19c9wjazjvnvmqgl4fsswwny110c";
    }
    {
      name = "shell-format";
      publisher = "foxundermoon";
      version = "7.2.5";
      sha256 = "0a874423xw7z6zjj7gzzl39jahrrqcf2r16zbcvncw23483m3yli";
    }
    {
      name = "go";
      publisher = "golang";
      version = "0.41.2";
      sha256 = "13fdnwgci87x1rdz1hwq52ling81c3l67cqvmckcjzri2r8gsgvq";
    }
    {
      name = "terraform";
      publisher = "hashicorp";
      version = "2.30.2024041014";
      sha256 = "sha256-Y0DsGSGkQxdT3BECWSO02G44gSt+nIU4RbJj0ErG3jU=";
    }
    {
      name = "nix-ide";
      publisher = "jnoortheen";
      version = "0.3.1";
      sha256 = "1cpfckh6zg8byi6x1llkdls24w9b0fvxx4qybi9zfcy5gc60r6nk";
    }
    {
      name = "alejandra";
      publisher = "kamadorueda";
      version = "1.0.0";
      sha256 = "1ncjzhrc27c3cwl2cblfjvfg23hdajasx8zkbnwx5wk6m2649s88";
    }
    {
      name = "bash-ide-vscode";
      publisher = "mads-hartmann";
      version = "1.39.0";
      sha256 = "1zand96p0wdpf8jjfpl53xjvzd1mgz33gfxdq941d7cil5kldnpj";
    }
    {
      name = "marp-vscode";
      publisher = "marp-team";
      version = "2.8.0";
      sha256 = "1rzmiz0026g0wkb3sq7mr60il0m9gsm8k9alhdbv8zwf0k9x3yf7";
    }
    {
      name = "dotenv";
      publisher = "mikestead";
      version = "1.0.1";
      sha256 = "0rs57csczwx6wrs99c442qpf6vllv2fby37f3a9rhwc8sg6849vn";
    }
    {
      name = "direnv";
      publisher = "mkhl";
      version = "0.17.0";
      sha256 = "1n2qdd1rspy6ar03yw7g7zy3yjg9j1xb5xa4v2q12b0y6dymrhgn";
    }
    {
      name = "ssh-config-syntax-highlighter";
      publisher = "mousavian";
      version = "0.0.5";
      sha256 = "0z63kn3z6f73kdhz1kz206f7fib95h5mwz4i6cril9kd3q59spd2";
    }
    {
      name = "vscode-docker";
      publisher = "ms-azuretools";
      version = "1.29.0";
      sha256 = "0rz32qwdf7a5hn3nnhxviaf8spwsszfrxmhnbbskspi5r9b6qm4r";
    }
    {
      name = "python";
      publisher = "ms-python";
      version = "2024.5.11021008";
      sha256 = "11mnnbdl7cqr18s2cvv2132rrq1f5zslnihp5i2jpa2awjak8wjj";
    }
    {
      name = "vscode-pylance";
      publisher = "ms-python";
      version = "2024.4.101";
      sha256 = "13yi6v1l7k1g0r7fhw3gasv7drn03slh0lxp6pcmqlrajkp0cn1n";
    }
    {
      name = "jupyter";
      publisher = "ms-toolsai";
      version = "2024.4.2024041101";
      sha256 = "sha256-R+xYn6ASOfPHVGdQFsJNZ02a2YYM68a6XBHLtF7ZYPE=";
    }
    {
      name = "jupyter-renderers";
      publisher = "ms-toolsai";
      version = "1.0.17";
      sha256 = "1c065s2cllf2x90i174qs2qyzywrlsjkc6agcc9qvdsb426c6r9l";
    }
    {
      name = "vsliveshare";
      publisher = "ms-vsliveshare";
      version = "1.0.5918";
      sha256 = "1m4mpy6irj3vzjw6mzmjjp6appgf000zfhmkjwxw65sl4wmjckaf";
    }
    {
      name = "color-highlight";
      publisher = "naumovs";
      version = "2.8.0";
      sha256 = "14capk3b7rs105ij4pjz42zsysdfnkwfjk9lj2cawnqxa7b8ygcr";
    }
    {
      name = "material-icon-theme";
      publisher = "pkief";
      version = "4.34.0";
      sha256 = "1ahshxw66436mc9jpiyfac0hinnqm3s0g3akybjrda13yd9884y7";
    }
    {
      name = "material-product-icons";
      publisher = "pkief";
      version = "1.7.0";
      sha256 = "1d0bz1yj09vsyv2k78xxx460n0zikb3g6w9w57l93mis2j82xaqp";
    }
    {
      name = "ansible";
      publisher = "redhat";
      version = "24.4.1526292";
      sha256 = "09l7rg3cfz4k5ijmwp5ak9iw0zvrsgmdaa5wrb153qjxx9nfyhk1";
    }
    {
      name = "vscode-yaml";
      publisher = "redhat";
      version = "1.14.0";
      sha256 = "0pww9qndd2vsizsibjsvscz9fbfx8srrj67x4vhmwr581q674944";
    }
    {
      name = "multi-command";
      publisher = "ryuta46";
      version = "1.6.0";
      sha256 = "0q97nvzi398fff1l4055nhpxr8n4pc9c983kjr8vcmpj1gbwsw82";
    }
    {
      name = "jinjahtml";
      publisher = "samuelcolvin";
      version = "0.20.0";
      sha256 = "14xam64dfhcq3mckw2s9bg58miw1v2jqfg1ays6ksz8b17fcn060";
    }
    {
      name = "trailing-spaces";
      publisher = "shardulm94";
      version = "0.4.1";
      sha256 = "15i1xcd7p6xfb8kj90irznf4xw48mmwzc528zrk3kiniy9nkbcd4";
    }
    {
      name = "even-better-toml";
      publisher = "tamasfe";
      version = "0.19.2";
      sha256 = "0q9z98i446cc8bw1h1mvrddn3dnpnm2gwmzwv2s3fxdni2ggma14";
    }
    {
      name = "ayu";
      publisher = "teabyii";
      version = "1.0.5";
      sha256 = "1visv44mizfvsysrdby1vrncv1g3qmf45rhjijmbyak2d60nm0gq";
    }
    {
      name = "pdf";
      publisher = "tomoki1207";
      version = "1.2.2";
      sha256 = "16rs255x569ahxldw8ra799w078h97aa2b11j97ipqgh6s5nax4b";
    }
    {
      name = "vscode-icons";
      publisher = "vscode-icons-team";
      version = "12.7.0";
      sha256 = "1w30gd0chf2c26a9c426ghs7gmss9dk9yzlrab51ydwhfkkd4hxb";
    }
    {
      name = "jinja";
      publisher = "wholroyd";
      version = "0.0.8";
      sha256 = "1ln9gly5bb7nvbziilnay4q448h9npdh7sd9xy277122h0qawkci";
    }
  ];
}
