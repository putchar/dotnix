{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customHomeManagerModules;
in {
  config = lib.mkIf cfg.i3.enable {
    home.packages = [
      pkgs.arandr
    ];

    programs.autorandr.enable = true;

    home.file = {
      ".screenlayout/vm.sh" = {
        text = ''
          #!/bin/sh
          xrandr --output Virtual-1 --primary --mode 1600x900 --pos 0x0 --rotate normal --output Virtual-2 --off --output Virtual-3 --off --output Virtual-4 --off
        '';
        executable = true;
      };
      ".screenlayout/laptop.sh" = {
        text = ''
          #!/bin/sh
          xrandr --output eDP-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output DP-1 --off --output HDMI-1 --off --output DP-2 --off --output HDMI-2 --off --output DP-2-8 --off --output DP-2-1 --off
        '';
        executable = true;
      };
      ".screenlayout/triple.sh" = {
        text = ''
          #!/bin/sh
          xrandr --output eDP-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output DP-1 --off --output HDMI-1 --off --output DP-2 --off --output HDMI-2 --off --output DP-2-8 --mode 2560x1440 --pos 1920x0 --rotate normal --output DP-2-1 --mode 1920x1080 --pos 4480x0 --rotate normal
        '';
        executable = true;
      };
      ".screenlayout/dell-single.sh" = {
        text = ''
          #!/bin/sh
          xrandr --output eDP-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output HDMI-1 --off --output DP-1 --off --output HDMI-2 --off --output DP-2 --off --output HDMI-3 --off --output DP-1-1 --off --output DP-1-2 --off --output DP-1-3 --off
        '';
        executable = true;
      };
      ".screenlayout/dell-dock.sh" = {
        text = ''
          #!/bin/sh
          xrandr --output eDP-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output HDMI-1 --off --output DP-1 --off --output HDMI-2 --off --output DP-2 --off --output HDMI-3 --off --output DP-1-1 --mode 1920x1080 --pos 1921x0 --rotate normal --output DP-1-2 --mode 1920x1080 --pos 3841x0 --rotate normal --output DP-1-3 --off
        '';
        executable = true;
      };
    };

    programs.bash = {
      shellAliases = {
        dell-single = "/home/putchar/.screenlayout/dell-single.sh";
        dell-dock = "/home/putchar/.screenlayout/dell-dock.sh";
        dual = "/home/putchar/.screenlayout/desktop.sh";
        single = "/home/putchar/.screenlayout/single.sh";
        laptop = "/home/putchar/.screenlayout/laptop.sh";
        triple = "/home/putchar/.screenlayout/triple.sh";
      };
    };
  };
}
