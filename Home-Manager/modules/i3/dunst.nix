{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customHomeManagerModules;
in {
  config = lib.mkIf cfg.i3.enable {
    home.packages = [
      pkgs.libnotify
      pkgs.numix-icon-theme-square
    ];

    services.dunst = {
      enable = true;
      iconTheme = {
        package = pkgs.numix-icon-theme-square;
        name = "Numix-Square";
      };
      settings = {
        global = {
          monitor = 0;
          follow = "mouse";
          geometry = "400x15-26+26";
          indicate_hidden = "yes";
          shrink = "no";
          transparency = 5;
          notification_height = 0;
          separator_height = 4;
          padding = 14;
          horizontal_padding = 8;
          frame_width = 3;
          corner_radius = 8;
          separator_color = "frame";
          sort = "yes";
          idle_threshold = 120;
          font = "Hack Nerd Font 9";
          line_height = 0;
          markup = "full";
          format = "<b>%s</b>\\n%b";
          alignment = "left";
          show_age_threshold = 60;
          word_wrap = "yes";
          ellipsize = "middle";
          ignore_newline = "no";
          stack_duplicates = true;
          hide_duplicate_count = false;
          show_indicators = "yes";
          icon_position = "left";
          max_icon_size = 32;
          sticky_history = "yes";
          history_length = 20;
          dmenu = "rofi -dmenu -p dunst:";
          browser = "firefox -new-tab";
          always_run_script = true;
          title = "Dunst";
          class = "Dunst";
          bounce_freq = 0;
          startup_notification = false;
          force_xinerama = false;
          frame_color = "#1a1c25";
        };

        experimental = {per_monitor_dpi = false;};

        shortcuts = {
          close = "ctrl+space";
          close_all = "ctrl+shift+space";
          history = "ctrl+grave";
          context = "ctrl+shift+period";
        };

        urgency_low = {
          background = "#1E2029";
          foreground = "#bbc2cf";
          timeout = 2;
        };

        urgency_normal = {
          background = "#2a2d39";
          foreground = "#bbc2cf";
          timeout = 2;
        };

        urgency_critical = {
          background = "#cc6666";
          foreground = "#1E2029";
          timeout = 3;
        };
      };

      #    iconTheme = "Numix";
      #pinentryFlavor = "qt";
    };
  };
}
