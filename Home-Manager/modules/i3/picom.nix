{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customHomeManagerModules;
in {
  config = lib.mkIf cfg.i3.enable {
    services.picom = {
      enable = true;
      blurExclude = ["window_type = 'dock'" "window_type = 'desktop'"];
      fade = true;
      fadeDelta = 4;
      fadeSteps = ["0.03" "0.03"];
      fadeExclude = [];
      shadow = false;
      shadowOffsets = [(-5) (-5)];
      shadowOpacity = "0.5";
      shadowExclude = [
        "! name~=''"
        "name = 'Notification'"
        "name = 'Plank'"
        "name = 'Docky'"
        "name = 'Kupfer'"
        "name = 'xfce4-notifyd'"
        "name *= 'VLC'"
        "name *= 'compton'"
        "name *= 'Chromium'"
        "name *= 'Chrome'"
        "name *= 'firefox'"
        "class_g = 'Conky'"
        "class_g = 'Kupfer'"
        "class_g = 'Synapse'"
        "class_g ?= 'Notify-osd'"
        "class_g ?= 'Cairo-dock'"
        "class_g ?= 'Xfce4-notifyd'"
        "class_g ?= 'Xfce4-power-manager'"
        "_GTK_FRAME_EXTENTS@:c"
      ];
      noDockShadow = true;
      noDNDShadow = true;
      activeOpacity = "1.0";
      inactiveDim = "0.0";
      inactiveOpacity = "1.0";
      menuOpacity = "1.0";
      opacityRule = [];
      backend = "glx";
      vSync = true;
      refreshRate = 0;
    };
  };
}
