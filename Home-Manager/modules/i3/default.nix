{
  config,
  pkgs,
  lib,
  ...
}: {
  imports = [
    ./arandr.nix
    ./conky.nix
    ./dunst.nix
    ./i3.nix
    ./rofi.nix
    ./thunar.nix
  ];

  options.customHomeManagerModules.i3 = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable i3 globally or not
      '';
    };
  };
}
