{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customHomeManagerModules;
in {
  options.customHomeManagerModules.conky = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable conky globally or not
      '';
    };
  };

  config = lib.mkIf cfg.i3.enable {
    home.packages = [
      pkgs.conky
    ];

    home.file.".config/conky/conky.conf".source = ./conky.conf;

    systemd.user.services."conky" = {
      Unit = {
        Description = "launch conky";
        After = ["graphical-session-pre.target"];
        PartOf = ["graphical-session.target"];
      };
      Install.WantedBy = ["graphical-session.target"];
      #Service.ExecStart = "/bin/sh -c '${pkgs.conky}/bin/conky -c $HOME/.config/conky/conky.conf'";
      Service.ExecStart = "${pkgs.conky}/bin/conky -c %h/.config/conky/conky.conf";
      Service.Environment = "DISPLAY=:0";
      Service.Type = "forking";
    };
  };
}
