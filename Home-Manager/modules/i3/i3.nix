{
  config,
  pkgs,
  lib,
  inputs,
  ...
}: let
  cfg = config.customHomeManagerModules;
  gopass = pkgs.gopass;
  mod = "Mod1";
  modeSystem = "System (l) lock, (e) logout, (s) suspend, (h) hibernate, (r) reboot, (Shift+s) shutdown, (Shift+r) BIOS";
  modeResize = "resize";
  ## Custom Workspace
  ## cf https://fontawesome.com/cheatsheet
  workspace1 = "1:Term ";
  workspace2 = "2:Web ";
  workspace3 = "3:IDE ";
  workspace4 = "4:Steam ";
  workspace5 = "5:Files ";
  workspace6 = "6:Virt  /";
  workspace7 = "7:Kindle 立";
  workspace8 = "8:Mail ";
  workspace9 = "9:WORK ";
  workspace10 = "10:Media ";
  colorLightBlack = "#2f343f";
  colorBlack = "#000000";
  colorLightGrey = "#525865";
  colorDarkGrey = "#222222";
  colorRed = "#bb0000";
  colorWhite = "#f3f4f5";
  colorGreen = "#00ff00";
in {
  config = lib.mkIf cfg.i3.enable {
    home.packages = [
      pkgs.barrier
      pkgs.pa_applet
      pkgs.playerctl
      pkgs.pavucontrol
      pkgs.envsubst
      pkgs.sysstat
      pkgs.wget
      pkgs.curl
      pkgs.tree
      pkgs.ncdu
      pkgs.ranger
      pkgs.htop
      pkgs.jq
      pkgs.yq
      pkgs.lsb-release
      pkgs.usbutils
      pkgs.pciutils
      pkgs.scrot
      pkgs.imagemagick
      pkgs.glib
      pkgs.xbindkeys
      pkgs.alacritty
      pkgs.polkit_gnome
      pkgs.at-spi2-core
      pkgs.dislocker
      pkgs.flameshot
      pkgs.feh
      pkgs.betterlockscreen
    ];

    services.screen-locker = {
      enable = true;
      #lockCmd = "/bin/sh -c '~/.config/i3/i3lock.sh'";
      lockCmd = "${pkgs.betterlockscreen}/bin/betterlockscreen --lock";
      #lockCmd = "betterlockscreen --lock";
      inactiveInterval = 1;
      xautolock.extraOptions = ["-corners 0-00"];
      xautolock.detectSleep = false;
    };

    #home.file.".config/betterlockscreenrc".source = ./betterlockscreenrc;

    #services.betterlockscreen = {
    #  enable = true;
    #  #inactiveInterval = 10;
    #  #package =
    #};

    xsession.enable = true;
    ## cf : https://rycee.gitlab.io/home-manager/options.html#opt-xsession.windowManager.i3.config.assigns
    xsession.windowManager.i3 = {
      enable = true;
      package = pkgs.i3-gaps;
      extraConfig = ''
        for_window [class="[Ss]potify"] move window to workspace $workspace10
        ## Custom Workspace
        set $workspace1  "${workspace1}"
        set $workspace2  "${workspace2}"
        set $workspace3  "${workspace3}"
        set $workspace4  "${workspace4}"
        set $workspace5  "${workspace5}"
        set $workspace6  "${workspace6}"
        set $workspace7  "${workspace7}"
        set $workspace8  "${workspace8}"
        set $workspace9  "${workspace9}"
        set $workspace10 "${workspace10}"
      '';
      config = {
        window.hideEdgeBorders = "both";
        window.titlebar = true;
        window.border = 2;
        floating.titlebar = true;
        floating.border = 2;
        defaultWorkspace = "${workspace1}";

        gaps.smartBorders = "on";
        modifier = mod;
        fonts = {
          names = ["Hack Nerd Font" "FontAwesome"];
          style = "Bold Semi-Condensed";
          size = 10.0;
        };

        colors = {
          background = "${colorWhite}";
          focused = {
            background = "${colorLightGrey}";
            border = "${colorBlack}";
            childBorder = "${colorLightGrey}";
            indicator = "${colorGreen}";
            text = "${colorWhite}";
          };
          focusedInactive = {
            background = "${colorDarkGrey}";
            border = "${colorBlack}";
            childBorder = "${colorDarkGrey}";
            indicator = "${colorGreen}";
            text = "${colorWhite}";
          };
          unfocused = {
            background = "${colorDarkGrey}";
            border = "${colorBlack}";
            childBorder = "${colorDarkGrey}";
            indicator = "${colorGreen}";
            text = "${colorWhite}";
          };
          urgent = {
            background = "${colorRed}";
            border = "${colorRed}";
            childBorder = "${colorDarkGrey}";
            indicator = "${colorGreen}";
            text = "${colorWhite}";
          };
          placeholder = {
            background = "${colorBlack}";
            border = "${colorBlack}";
            childBorder = "${colorDarkGrey}";
            indicator = "${colorBlack}";
            text = "${colorWhite}";
          };
        };

        bars = [
          {
            position = "top";
            fonts = {
              names = ["Hack Nerd Font" "FontAwesome"];
              style = "Bold";
              size = 10.0;
            };
            statusCommand = "i3blocks -c ~/.config/i3/i3blocks.conf";
            colors = {
              background = "${colorDarkGrey}";
              separator = "${colorWhite}";
              activeWorkspace = {
                background = "${colorDarkGrey}";
                border = "${colorDarkGrey}";
                text = "${colorWhite}";
              };
              inactiveWorkspace = {
                background = "${colorDarkGrey}";
                border = "${colorDarkGrey}";
                text = "${colorWhite}";
              };
              focusedWorkspace = {
                background = "${colorLightGrey}";
                border = "${colorDarkGrey}";
                text = "${colorWhite}";
              };
              bindingMode = {
                background = "${colorRed}";
                border = "${colorRed}";
                text = "${colorWhite}";
              };
              urgentWorkspace = {
                background = "${colorRed}";
                border = "${colorRed}";
                text = "${colorWhite}";
              };
            };
          }
        ];

        keybindings = lib.mkOptionDefault {
          "${mod}+Return" = "exec ${pkgs.alacritty}/bin/alacritty";
          # Focus
          "${mod}+Left" = "focus left";
          "${mod}+Down" = "focus down";
          "${mod}+Up" = "focus up";
          "${mod}+Right" = "focus right";

          "${mod}+j" = "focus left";
          "${mod}+k" = "focus down";
          "${mod}+i" = "focus up";
          "${mod}+l" = "focus right";

          "${mod}+Shift+j" = "move left";
          "${mod}+Shift+k" = "move down";
          "${mod}+Shift+i" = "move up";
          "${mod}+Shift+l" = "move right";

          "${mod}+Shift+Left" = "move left";
          "${mod}+Shift+Down" = "move down";
          "${mod}+Shift+Up" = "move up";
          "${mod}+Shift+Right" = "move right";

          "${mod}+Control+l" = "exec ${pkgs.systemd}/bin/loginctl lock-session $XDG_SESSION_ID";
          "${mod}+d" = ''
            exec "rofi -show drun -theme-str 'element-icon { size: 2.2ch;}'"'';

          ## gopass wrapper
          "${mod}+g" = "exec ${gopass}/bin/gopass ls --flat | rofi -dmenu | xargs --no-run-if-empty ${gopass}/bin/gopass show -c";
          "${mod}+Shift+g" = "exec ${gopass}/bin/gopass ls --flat | rofi -dmenu | xargs --no-run-if-empty ${gopass}/bin/gopass show -f | head -n 1 | xdotool type --clearmodifiers --file -";

          "${mod}+equal" = "exec playerctl next";
          "${mod}+minus" = "exec playerctl previous";
          "XF86AudioNext" = "exec playerctl next";
          "XF86AudioPrev" = "exec playerctl previous";
          "XF86AudioPlay" = "exec playerctl play-pause";
          "Print" = "exec flameshot gui";
          "${mod}+1" = "workspace $workspace1";
          "${mod}+2" = "workspace $workspace2";
          "${mod}+3" = "workspace $workspace3";
          "${mod}+4" = "workspace $workspace4";
          "${mod}+5" = "workspace $workspace5";
          "${mod}+6" = "workspace $workspace6";
          "${mod}+7" = "workspace $workspace7";
          "${mod}+8" = "workspace $workspace8";
          "${mod}+9" = "workspace $workspace9";
          "${mod}+0" = "workspace $workspace10";
          "${mod}+Shift+1" = "move container to workspace $workspace1";
          "${mod}+Shift+2" = "move container to workspace $workspace2";
          "${mod}+Shift+3" = "move container to workspace $workspace3";
          "${mod}+Shift+4" = "move container to workspace $workspace4";
          "${mod}+Shift+5" = "move container to workspace $workspace5";
          "${mod}+Shift+6" = "move container to workspace $workspace6";
          "${mod}+Shift+7" = "move container to workspace $workspace7";
          "${mod}+Shift+8" = "move container to workspace $workspace8";
          "${mod}+Shift+9" = "move container to workspace $workspace9";
          "${mod}+Shift+0" = "move container to workspace $workspace10";

          "${mod}+c" = "reload";
          "${mod}+Shift+e" = "exec i3-nagbar -t warning -m 'Do you want to exit i3?' -b 'Yes' 'i3-msg exit'";
          "${mod}+Shift+p" = "move scratchpad";
          "${mod}+p" = "scratchpad show";
          "${mod}+Shift+q" = "kill";
          "${mod}+Shift+r" = "restart";
          "${mod}+Shift+space" = "floating toggle";

          "${mod}+x" = "move workspace to output right";
          "${mod}+r" = ''mode "${modeResize}"'';
          "${mod}+Shift+t" = ''mode "${modeSystem}"'';
        };
        assigns = {
          "${workspace1}" = [{class = "Terminator";}];
          "${workspace2}" = [
            {class = "[Cc]hromium";}
            {class = "[Ff]irefox";}
            {class = "[Ss]lack";}
            {class = "[Ss]ignal";}
            {class = "[Gg]oogle-chrome";}
            {class = "[Dd]iscord";}
            {class = "[Ss]kype";}
          ];
          "${workspace3}" = [
            {class = "[Aa]tom";}
            {class = "[Vv]SCodium";}
            {class = "[Cc]ode-oss";}
            {class = "[Cc]ode";}
            {class = "[Vv]scode";}
          ];
          "${workspace4}" = [{class = "[Ss]team";}];
          "${workspace5}" = [
            {class = "[Tt]hunar";}
            {class = ".thunar-wrapped_";}
            {class = "[Ff]ilezilla";}
            {class = "[Ll]ibre[Oo]ffice";}
          ];
          "${workspace6}" = [
            {class = "[Vv]irt-manager";}
            {class = ".virt-manager-wrapped";}
            {class = "[Rr]emmina";}
          ];
          "${workspace8}" = [{class = "[Tt]hunderbird";} {class = "[Mm]ailspring";}];
          "${workspace9}" = [{class = "KeePassXC";}];
          "${workspace10}" = [
            {class = "[Pp]avucontrol";}
            {class = "[Ss]potify";}
            {class = ".blueman-manager-wrapped";}
          ];
        };

        modes."${modeSystem}" = {
          "l" = ''
            exec --no-startup-id i3lock --color '${colorDarkGrey}', mode "default"'';
          "e" = ''exec --no-startup-id i3-msg exit, mode "default"'';
          "s" = ''
            exec --no-startup-id i3lock --color '${colorDarkGrey}' && sleep 1 && systemctl suspend, mode "default"'';
          "h" = ''
            exec --no-startup-id i3lock --color '${colorDarkGrey}' && sleep 1 && systemctl hibernate, mode "default"'';
          "r" = ''exec --no-startup-id systemctl reboot, mode "default"'';
          "Shift+s" = ''exec --no-startup-id systemctl poweroff -i, mode "default"'';
          "Shift+r" = ''
            exec --no-startup-id systemctl reboot --firmware-setup, mode "default"'';

          # back to normal: Enter or Escape
          "Return" = ''mode "default"'';
          "Escape" = ''mode "default"'';
        };
        modes."${modeResize}" = {
          "j" = "resize shrink width 10 px or 10 ppt";
          "k" = "resize grow height 10 px or 10 ppt";
          "i" = "resize shrink height 10 px or 10 ppt";
          "l" = "resize grow width 10 px or 10 ppt";
          "Left" = "resize shrink width 10 px or 10 ppt";
          "Down" = "resize grow height 10 px or 10 ppt";
          "Up" = "resize shrink height 10 px or 10 ppt";
          "Right" = "resize grow width 10 px or 10 ppt";

          # back to normal: Enter or Escape or $mod+r
          "Return" = ''mode "default"'';
          "Escape" = ''mode "default"'';
          "${mod}+r" = ''mode "default"'';
        };
      };
    };

    #home.file.".config/i3/i3blocks.conf".source = ./i3blocks.conf;
    home.file.".config/i3/i3blocks.conf".text = ''
      # i3blocks config file
      #
      # Please see man i3blocks for a complete reference!
      # The man page is also hosted at http://vivien.github.io/i3blocks


      # Global properties
      #
      # The top properties below are applied to every block, but can be overridden.
      # Each block command defaults to the script name to avoid boilerplate.
      command=~/.config/i3/i3blocks-contrib/$BLOCK_NAME/$BLOCK_NAME
      separator_block_width=15
      markup=none

      [mediaplayer]
      label= :
      instance=spotify
      interval=10
      signal=10

      # CPU usage
      #
      # The script may be called with -w and -c switches to specify thresholds,
      # see the script for details.
      [load_average]
      label= :
      interval=10
      separator=false

      [cpu_usage]
      label= :
      interval=10
      min_width=CPU:100.00%
      separator=false

      # Temperature
      #
      # Support multiple chips, though lm-sensors.
      # The script may be called with -w and -c switches to specify thresholds,
      # see the script for details.
      [temperature]
      label= :
      interval=10

      # Memory usage
      #
      # The type defaults to "mem" if the instance is not specified.
      [memory]
      label= :
      separator=false
      interval=30

      [memory]
      label= :
      instance=swap
      separator=false
      interval=30

      # Disk usage
      #
      # The directory defaults to $HOME if the instance is not specified.
      # The script may be called with a optional argument to set the alert
      # (defaults to 10 for 10%).
      [disk]
      label= :
      #instance=/mnt/data
      interval=30

      [battery]
      interval=30
      label= :
      #command=~/Documents/github.com/i3blocks-contrib/$BLOCK_NAME/$BLOCK_NAME
      #command=acpi -b | awk '{print $4}' | cut -d % -f 1 | tail -n 1
      #command=~/Documents/gitlab.com/helper/battery.py
      #command=~/Documents/gitlab.com/helper/toto.py

      # Network interface monitoring
      #
      # If the instance is not specified, use the interface used for default route.
      # The address can be forced to IPv4 or IPv6 with -4 or -6 switches.
      [iface]
      label= :
      #instance=enp9s0
      instance=eno1
      color=#00FF00
      interval=10
      separator=false

      [iface]
      label= :
      instance=wlp4s0
      color=#00FF00
      interval=10
      separator=false

      [bandwidth]
      #label= :
      interval=5
      INLABEL=:
      OUTLABEL=:
      #interval=persist
      markup=pango
      #WARN_RX=307200
      #WARN_TX=30720
      #CRIT_RX=512000
      #CRIT_TX=51200

      # Date Time
      #
      [time]
      command=date '+%d-%m-%Y %H:%M:%S'
      interval=1
    '';

    home.file.".config/i3/i3lock.sh".text = ''
      #!/usr/bin/env /run/current-system/sw/bin/sh

      USER_BIN_PATH="/home/putchar/.nix-profile/bin"
      GLOBAL_BIN_PATH="/run/current-system/sw/bin"
      IMAGE=/tmp/i3lock.png
      BLURTYPE="3x3"
      BLURTYPE="4x4"

      $USER_BIN_PATH/scrot -z $IMAGE
      $USER_BIN_PATH/convert $IMAGE -blur $BLURTYPE $IMAGE

      $GLOBAL_BIN_PATH/i3lock -n -i $IMAGE
      $GLOBAL_BIN_PATH/rm $IMAGE
    '';

    systemd.user.services."i3blocks-contrib-pull" = {
      Install.WantedBy = ["network-online.target"];
      Service.Type = "simple";
      Service.ExecStart = "/bin/sh -c '${pkgs.coreutils}/bin/test -d $HOME/.config/i3/i3blocks-contrib/.git && ${pkgs.git}/bin/git --git-dir=$HOME/.config/i3/i3blocks-contrib/.git pull || ${pkgs.git}/bin/git clone https://github.com/vivien/i3blocks-contrib.git $HOME/.config/i3/i3blocks-contrib'";
      Unit = {
        Description = "pull repo";
        Wants = ["network-online.target"];
        After = ["network-online.target"];
        #PartOf = [ "graphical-session.target" ];
      };
    };

    systemd.user.services.polkit-gnome = {
      Unit = {
        Description = "PolicyKit Authentication Agent";
        After = ["graphical-session-pre.target"];
        PartOf = ["graphical-session.target"];
      };
      Service = {
        ExecStart = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
      };
      Install = {WantedBy = ["graphical-session.target"];};
    };

    systemd.user.services.pa-applet = {
      Unit = {
        Description = "pa-applet";
        After = ["graphical-session-pre.target"];
        PartOf = ["graphical-session.target"];
      };
      Service = {ExecStart = "${pkgs.pa_applet}/bin/pa-applet";};
      Install = {WantedBy = ["graphical-session.target"];};
    };

    systemd.user.services.flameshot-start = {
      Unit = {
        Description = "flameshot start";
        After = ["graphical-session-pre.target"];
        PartOf = ["graphical-session.target"];
      };
      Service = {ExecStart = "${pkgs.flameshot}/bin/flameshot";};
      Install = {WantedBy = ["graphical-session.target"];};
    };
  };
}
