{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customHomeManagerModules;
in {
  config = lib.mkIf cfg.sway.enable {
    home.packages = [
      pkgs.yambar
    ];
    #home.file."config/yambar/config.yml".source = ./;
    home.file.".config/yambar/config.yml".text = ''
      bar:
        height: 26
        location: top
        spacing: 5
        margin: 7
        #background: 122129ff
        # Default font
        font: "Hack Nerd Font:style=Bold:size=8"
        #foreground: 122129ff
        #background: 122129ff
        background: 1e222aff
        border:
          width: 1
          color: 999999cc
          margin: 5
          top-margin: 0

        left:
          - i3:
              content:
                "":
                  map:
                    tag: state
                    default:
                      string:
                        text: "{name}"
                        font:  "Hack Nerd Font:style=Bold:size=11"
                    values:
                      focused:
                        string:
                          text: "{name}*"
                          font:  "Hack Nerd Font:style=Bold:size=11"
                current:
                  string:
                    text: "{application}: {title}"
                    font:  "Hack Nerd Font:style=Bold:size=11"
        right:
          - clock:
              content:
                - string:
                    text: ""
                    font: "Hack Nerd Font:style=Bold:size=14"

                - string:
                    text: " : {date}"
                    font:  "Hack Nerd Font:style=Bold:size=12"
                    right-margin: 5

                - string:
                    text: ""
                    font: "Hack Nerd Font:style=Bold:size=14"

                - string:
                    text: " : {time}"
                    font:  "Hack Nerd Font:style=Bold:size=12"

    '';
  };
}
