{
  config,
  pkgs,
  lib,
  inputs,
  ...
}: let
  cfg = config.customHomeManagerModules;
  gopass = "${pkgs.gopass}/bin/gopass";
  swaylock = "${pkgs.swaylock}/bin/swaylock";
  swaymsg = "${pkgs.sway}/bin/swaymsg";
  alacritty = "${pkgs.alacritty}/bin/alacritty";
  waybar = "${pkgs.waybar}/bin/waybar";
  loginctl = "${pkgs.systemd}/bin/loginctl";
  rofi-wayland = "${pkgs.rofi-wayland}/bin/rofi";
  playerctl = "${pkgs.playerctl}/bin/playerctl";
  brightnessctl = "${pkgs.brightnessctl}/bin/brightnessctl";
  pactl = "${pkgs.pulseaudio}/bin/pactl";
  wpctl = "${pkgs.wireplumber}/bin/wpctl";
  grim = "${pkgs.grim}/bin/grim";
  slurp = "${pkgs.slurp}/bin/slurp";
  swappy = "${pkgs.swappy}/bin/swappy";
  systemctl = "${pkgs.systemd}/bin/systemctl";
  notify-send = "${pkgs.libnotify}/bin/notify-send";
  xargs = "${pkgs.findutils}/bin/xargs";
  head = "${pkgs.coreutils}/bin/head";
  xdotool = "${pkgs.xdotool}/bin/xdotool";
  swayProp = "${pkgs_swayProp}/bin/swayProp";

  waylandEnv = {
    CLUTTER_BACKEND = "wayland";
    SDL_VIDEODRIVER = "wayland";
    QT_QPA_PLATFORM = "wayland";
    QT_WAYLAND_DISABLE_WINDOWDECORATION = "1";
    _JAVA_AWT_WM_NONREPARENTING = "1";
    MOZ_ENABLE_WAYLAND = "1";
    XDG_SESSION_TYPE = "wayland";
    XDG_SESSION_DESKTOP = "sway";
    XDG_CURRENT_DESKTOP = "sway";
    WLR_NO_HARDWARE_CURSORS = "1";
    #NIXOS_OZONE_WL = "1";
    #GTK_USE_PORTAL = "1";
  };
  wallpaperPath = "/home/putchar/Documents/gitlab.com/putchar/dotnix/Home-Manager/modules/wallpaper";
  mod = "Mod1";
  modeSystem = "System (l) lock, (e) logout, (s) suspend, (h) hibernate, (r) reboot, (Shift+s) shutdown, (Shift+r) BIOS";
  modeResize = "Resize";
  ## Custom Workspace
  ## cf https://fontawesome.com/cheatsheet
  workspace1 = "1:Term ";
  workspace2 = "2:Web ";
  workspace3 = "3:IDE ";
  workspace4 = "4:Steam ";
  workspace5 = "5:Files ";
  workspace6 = "6:Virt  /";
  workspace7 = "7:Kindle 立";
  workspace8 = "8:Mail ";
  workspace9 = "9:WORK ";
  workspace10 = "10:Media ";
  colorLightBlack = "#2f343f";
  colorBlack = "#000000";
  colorLightGrey = "#525865";
  colorDarkGrey = "#222222";
  colorRed = "#bb0000";
  colorWhite = "#f3f4f5";
  colorGreen = "#00ff00";

  pkgs_swayProp = pkgs.writeShellScriptBin "swayProp" ''
    ## cf https://gitlab.com/wef/dotfiles/-/blob/master/bin/sway-prop
    PROG=$( basename $0 )

    case "$1" in
        -h|--help)
            echo "Usage: $PROG"
            echo
            echo 'shows the properties of the focused window

    best bindings:
    bindsym  $mod+question exec sway-prop
    bindsym $a+$c+question exec sway-prop

    but if running from a terminal rather than a sway key binding:
    sleep 2; sway-prop'
            exit 0
            ;;
    esac

    TMP=/tmp/sway-prop.tmp

    trap "rm $TMP" EXIT

    ${swaymsg} -t get_tree | jq '.. | select(.type?) | select(.focused==true)' > "$TMP"
    ${alacritty} --class floating -e bash -c "less $TMP"
  '';
in {
  config = lib.mkIf cfg.sway.enable {
    ## shrug https://github.com/nix-community/home-manager/issues/5311#issuecomment-2068042917
    wayland.windowManager.sway.checkConfig = false;

    ## https://nix-community.github.io/home-manager/options.html#opt-services.gnome-keyring.enable
    services.gnome-keyring.enable = true;

    systemd.user.services.polkit-gnome = {
      Unit = {
        Description = "PolicyKit Authentication Agent";
        After = ["graphical-session-pre.target"];
        PartOf = ["graphical-session.target"];
      };
      Service = {
        ExecStart = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
      };
      Install = {
        WantedBy = ["graphical-session.target"];
      };
    };

    #    systemd.user.services.tailscale-systray = {
    #      Unit = {
    #        Description = "tailscale-systray";
    #        After = ["graphical-session-pre.target"];
    #        PartOf = ["graphical-session.target"];
    #      };
    #      Service = {
    #        ExecStart = "${pkgs.tailscale-systray}/bin/tailscale-systray";
    #      };
    #      Install = {
    #        WantedBy = ["graphical-session.target"];
    #      };
    #    };

    home.packages = [
      pkgs.pavucontrol
      pkgs.pulseaudio
      pkgs.numix-cursor-theme
      pkgs.playerctl
      pkgs.wev
      pkgs.jq
      pkgs_swayProp
      pkgs.wlprop
      pkgs.wf-recorder
      ## TODO https://github.com/milgra/sov
      #(pkgs.callPackage ./sov.nix {inherit inputs;})
    ];
    home.sessionVariables = waylandEnv;

    services.gammastep = {
      enable = true;
      dawnTime = "6:00-7:45";
      duskTime = "18:35-20:45";
      latitude = 48.9;
      longitude = 2.26;
      provider = "manual";
      tray = true;
      ## https://nix-community.github.io/home-manager/options.html#opt-services.gammastep.settings
      #settings
    };
    ## https://git.sbruder.de/simon/nixos-config/src/branch/master/users/simon/modules/sway/default.nix#L242
    ## https://wiki.archlinux.org/title/Sway#Input_devices
    ## https://wiki.archlinux.org/title/Sway#Idle
    ## https://nix-community.github.io/home-manager/options.html#opt-services.swayidle.events
    services.swayidle = {
      enable = true;
      events = [
        {
          event = "lock";
          command = "${swaylock} -f --image '${wallpaperPath}/night.png'";
        }
      ];
      timeouts = [
        {
          timeout = 60;
          command = "${swaylock} -f --image '${wallpaperPath}/night.png'";
        }
        {
          timeout = 90;
          command = "${swaymsg} 'output * dpms off'";
          resumeCommand = "${swaymsg} 'output * dpms on'";
        }
      ];
    };

    ## https://wiki.archlinux.org/title/Sway#Manage_Sway-specific_daemons_with_systemd
    ## https://nix-community.github.io/home-manager/options.html#opt-wayland.windowManager.sway.enable
    wayland.windowManager.sway = {
      enable = true;
      wrapperFeatures.base = true;
      wrapperFeatures.gtk = true;
      systemd.enable = true;
      swaynag.enable = true;
      xwayland = true;
      extraSessionCommands = ''
        export CLUTTER_BACKEND="wayland"
        export SDL_VIDEODRIVER="wayland"
        export QT_QPA_PLATFORM="wayland"
        export QT_WAYLAND_DISABLE_WINDOWDECORATION="1"
        export _JAVA_AWT_WM_NONREPARENTING="1"
        export MOZ_ENABLE_WAYLAND="1"
        export XDG_SESSION_TYPE="wayland"
        export XDG_SESSION_DESKTOP="sway"
        export XDG_CURRENT_DESKTOP="sway"
        export WLR_NO_HARDWARE_CURSORS="1"
        #export NIXOS_OZONE_WL="1"
        #export GTK_USE_PORTAL="1"
      '';
      extraConfig = ''
        for_window [class="[Ss]potify"] move window to workspace $workspace10
        ## Custom Workspace
        set $workspace1  ${workspace1}
        set $workspace2  ${workspace2}
        set $workspace3  ${workspace3}
        set $workspace4  ${workspace4}
        set $workspace5  ${workspace5}
        set $workspace6  ${workspace6}
        set $workspace7  ${workspace7}
        set $workspace8  ${workspace8}
        set $workspace9  ${workspace9}
        set $workspace10 ${workspace10}

        include /etc/sway/config.d/*
      '';
      config = {
        window.hideEdgeBorders = "none";
        window.titlebar = false;
        window.border = 2;
        floating.titlebar = true;
        floating.border = 2;
        defaultWorkspace = "${workspace1}";
        terminal = "${alacritty}";
        #startup = [{command = "${pkgs.tailscale-systray}/bin/tailscale-systray";}];
        input = {
          "type:keyboard" = {
            xkb_layout = "us";
            xkb_variant = "intl";
          };
          "type:touchpad" = {
            tap = "enabled";
            #left_handed = "enabled";
            #natural_scroll = "disabled";
            #dwt = "enabled";
            accel_profile = "adaptive"; # disable mouse acceleration (enabled by default; to set it manually, use "adaptive" instead of "flat")
            pointer_accel = "0.3"; # set mouse sensitivity (between -1 and 1)
          };
        };

        ## cf https://github.com/colemickens/nixcfg/blob/main/mixins/sway.nix
        output = {
          "*" = {
            bg = "${wallpaperPath}/fractal.jpeg fill";
            subpixel = "rgb";
            #adaptive_sync = "on";
          };
          "HDMI-A-1" = {pos = "0 0";};
          "DP-1" = {pos = "1920 0";};
          ## for my framework
          "eDP-1" = {
            scale = "1.0";
          };
        };

        gaps = {
          #  bottom = 5;
          #  horizontal = 5;
          #  inner = 5;
          #  left = 5;
          #  outer = 5;
          #  right = 5;
          smartBorders = "on";
          smartGaps = false;
          #  top = 5;
          #  vertical = 5;
        };

        fonts = {
          names = ["Hack Nerd Font" "FontAwesome"];
          style = "Bold";
          size = 9.0;
        };

        colors = {
          background = "${colorWhite}";
          focused = {
            background = "${colorLightGrey}";
            border = "${colorBlack}";
            childBorder = "${colorLightGrey}";
            indicator = "${colorGreen}";
            text = "${colorWhite}";
          };
          focusedInactive = {
            background = "${colorDarkGrey}";
            border = "${colorBlack}";
            childBorder = "${colorDarkGrey}";
            indicator = "${colorGreen}";
            text = "${colorWhite}";
          };
          unfocused = {
            background = "${colorDarkGrey}";
            border = "${colorBlack}";
            childBorder = "${colorDarkGrey}";
            indicator = "${colorGreen}";
            text = "${colorWhite}";
          };
          urgent = {
            background = "${colorRed}";
            border = "${colorRed}";
            childBorder = "${colorDarkGrey}";
            indicator = "${colorGreen}";
            text = "${colorWhite}";
          };
          placeholder = {
            background = "${colorBlack}";
            border = "${colorBlack}";
            childBorder = "${colorDarkGrey}";
            indicator = "${colorBlack}";
            text = "${colorWhite}";
          };
        };

        bars = [
          {
            position = "top";
            command = "${waybar}";
            fonts = {
              names = ["Hack Nerd Font" "FontAwesome"];
              style = "Bold";
              size = 9.0;
            };
            #statusCommand = "i3blocks -c ~/.config/i3/i3blocks.conf";
            colors = {
              background = "${colorDarkGrey}";
              separator = "${colorWhite}";
              activeWorkspace = {
                background = "${colorDarkGrey}";
                border = "${colorDarkGrey}";
                text = "${colorWhite}";
              };
              inactiveWorkspace = {
                background = "${colorDarkGrey}";
                border = "${colorDarkGrey}";
                text = "${colorWhite}";
              };
              focusedWorkspace = {
                background = "${colorLightGrey}";
                border = "${colorDarkGrey}";
                text = "${colorWhite}";
              };
              bindingMode = {
                background = "${colorRed}";
                border = "${colorRed}";
                text = "${colorWhite}";
              };
              urgentWorkspace = {
                background = "${colorRed}";
                border = "${colorRed}";
                text = "${colorWhite}";
              };
            };
          }
        ];

        keybindings = lib.mkOptionDefault {
          "${mod}+Return" = "exec ${alacritty}";
          # Focus
          "${mod}+Left" = "focus left";
          "${mod}+Down" = "focus down";
          "${mod}+Up" = "focus up";
          "${mod}+Right" = "focus right";

          "${mod}+j" = "focus left";
          "${mod}+k" = "focus down";
          "${mod}+i" = "focus up";
          "${mod}+l" = "focus right";

          "${mod}+Shift+j" = "move left";
          "${mod}+Shift+k" = "move down";
          "${mod}+Shift+i" = "move up";
          "${mod}+Shift+l" = "move right";

          "${mod}+Shift+Left" = "move left";
          "${mod}+Shift+Down" = "move down";
          "${mod}+Shift+Up" = "move up";
          "${mod}+Shift+Right" = "move right";

          "${mod}+Control+l" = "exec ${loginctl} lock-session $XDG_SESSION_ID";
          "${mod}+d" = ''
            exec "${rofi-wayland} -show drun -theme-str 'element-icon { size: 2.2ch;}'"
          '';

          ## gopass wrapper https://github.com/gopasspw/gopass/blob/master/docs/setup.md#dmenu--rofi-support
          "${mod}+g" = "exec ${gopass} ls --flat | ${rofi-wayland} -dmenu | ${xargs} --no-run-if-empty ${gopass} show -c";
          "${mod}+Shift+g" = "exec ${gopass} ls --flat | ${rofi-wayland} -dmenu | ${xargs} --no-run-if-empty ${gopass} show -f | ${head} -n 1 | ${xdotool} type --clearmodifiers --file -";

          # Brightness
          "XF86MonBrightnessDown" = "exec ${brightnessctl} set 10%-";
          "XF86MonBrightnessUp" = "exec ${brightnessctl} set +10%";

          ## To allow a keybinding to be executed while the lockscreen is active add the --locked parameter to bindsym.
          # Audio
          "--locked ${mod}+equal" = "exec ${playerctl} next";
          "--locked ${mod}+minus" = "exec ${playerctl} previous";
          "--locked XF86AudioNext" = "exec ${playerctl} next";
          "--locked XF86AudioPrev" = "exec ${playerctl} previous";
          "--locked XF86AudioPlay" = "exec ${playerctl} play-pause";
          # Volume
          "--locked XF86AudioRaiseVolume" = "exec ${wpctl} set-volume @DEFAULT_SINK@ 3%+ && ${notify-send} '󰕾 +3%'";
          "--locked XF86AudioLowerVolume" = "exec ${wpctl} set-volume @DEFAULT_SINK@ 3%- && ${notify-send} '󰕾 -3%'";
          "--locked XF86AudioMute" = "exec ${wpctl} set-mute @DEFAULT_SINK@ toggle";
          "Print" = ''
            exec ${grim} -g "$(${slurp})" - | ${swappy} -f -
          '';
          "${mod}+1" = "workspace $workspace1";
          "${mod}+2" = "workspace $workspace2";
          "${mod}+3" = "workspace $workspace3";
          "${mod}+4" = "workspace $workspace4";
          "${mod}+5" = "workspace $workspace5";
          "${mod}+6" = "workspace $workspace6";
          "${mod}+7" = "workspace $workspace7";
          "${mod}+8" = "workspace $workspace8";
          "${mod}+9" = "workspace $workspace9";
          "${mod}+0" = "workspace $workspace10";
          "${mod}+Shift+1" = "move container to workspace $workspace1";
          "${mod}+Shift+2" = "move container to workspace $workspace2";
          "${mod}+Shift+3" = "move container to workspace $workspace3";
          "${mod}+Shift+4" = "move container to workspace $workspace4";
          "${mod}+Shift+5" = "move container to workspace $workspace5";
          "${mod}+Shift+6" = "move container to workspace $workspace6";
          "${mod}+Shift+7" = "move container to workspace $workspace7";
          "${mod}+Shift+8" = "move container to workspace $workspace8";
          "${mod}+Shift+9" = "move container to workspace $workspace9";
          "${mod}+Shift+0" = "move container to workspace $workspace10";

          "${mod}+Shift+p" = "move scratchpad";
          "${mod}+p" = "scratchpad show";
          "${mod}+Shift+q" = "kill";
          "${mod}+c" = "reload";
          "${mod}+Shift+r" = "restart";
          "${mod}+Shift+space" = "floating toggle";

          "${mod}+x" = "move workspace to output right";
          "${mod}+r" = ''mode "${modeResize}"'';
          "${mod}+Shift+t" = ''mode "${modeSystem}"'';
          #"${mod}+Shift+v" = "exec ${swayProp}";
          "${mod}+Shift+v" = "exec ${pkgs.wlprop}/bin/wlprop";
        };

        assigns = {
          "${workspace1}" = [
            {app_id = "Terminator";}
            {class = "Terminator";}
          ];
          "${workspace2}" = [
            {app_id = "[Cc]hromium";}
            {class = "[Cc]hromium";}
            {app_id = "[Ff]irefox";}
            {class = "[Ff]irefox";}
            {app_id = "[Ss]lack";}
            {class = "[Ss]lack";}
            {app_id = "[Ss]ignal";}
            {class = "[Ss]ignal";}
            {app_id = "[Gg]oogle-chrome";}
            {class = "[Gg]oogle-chrome";}
            {app_id = "[Dd]iscord";}
            {class = "[Dd]iscord";}
            {app_id = "[Ss]kype";}
            {class = "[Ss]kype";}
          ];
          "${workspace3}" = [
            {app_id = "[Vv]SCodium";}
            {class = "[Vv]SCodium";}
            {app_id = "[Cc]ode-oss";}
            {class = "[Cc]ode-oss";}
            {app_id = "[Cc]ode";}
            {class = "[Cc]ode";}
            {app_id = "[Vv]scode";}
            {class = "[Vv]scode";}
            {app_id = "[Cc]ode-url-handler";}
            {class = "[Cc]ode-url-handler";}
            {app_id = "[Dd][Bb]eaver";}
            {class = "[Dd][Bb]eaver";}
          ];
          "${workspace4}" = [
            {app_id = "[Ss]team";}
            {class = "[Ss]team";}
          ];
          "${workspace5}" = [
            {app_id = "[Tt]hunar";}
            {class = "[Tt]hunar";}
            {app_id = ".thunar-wrapped_";}
            {class = ".thunar-wrapped_";}
            {app_id = "[Ff]ilezilla";}
            {class = "[Ff]ilezilla";}
            {app_id = "[Ll]ibre[Oo]ffice";}
            {class = "[Ll]ibre[Oo]ffice";}
          ];
          "${workspace6}" = [
            {app_id = "[Vv]irt-manager";}
            {class = "[Vv]irt-manager";}
            {app_id = ".virt-manager-wrapped";}
            {class = ".virt-manager-wrapped";}
            {app_id = "[Rr]emmina";}
            {class = "[Rr]emmina";}
            {app_id = "[Vv]irtualBox Manager";}
            {class = "[Vv]irtualBox Manager";}
          ];
          "${workspace8}" = [
            {app_id = "[Tt]hunderbird";}
            {class = "[Tt]hunderbird";}
            {app_id = "[Mm]ailspring";}
            {class = "[Mm]ailspring";}
          ];
          "${workspace9}" = [
            {app_id = "KeePassXC";}
            {class = "KeePassXC";}
          ];
          "${workspace10}" = [
            {app_id = "[Pp]avucontrol";}
            {class = "[Pp]avucontrol";}
            {app_id = "[Ss]potify";}
            {class = "[Ss]potify";}
            {app_id = ".blueman-manager-wrapped";}
            {class = ".blueman-manager-wrapped";}
          ];
        };

        modes = {
          "${modeSystem}" = {
            "l" = ''exec --no-startup-id ${swaylock} --color '${colorDarkGrey}', mode "default"'';
            "e" = ''exec --no-startup-id ${swaymsg} exit, mode "default"'';
            "s" = ''exec --no-startup-id ${swaylock} --color '${colorDarkGrey}' && sleep 1 && ${systemctl} suspend, mode "default"'';
            "h" = ''exec --no-startup-id ${systemctl} hibernate, mode "default"'';
            "r" = ''exec --no-startup-id ${systemctl} reboot, mode "default"'';
            "Shift+s" = ''exec --no-startup-id ${systemctl} poweroff -i, mode "default"'';
            "Shift+r" = ''exec --no-startup-id ${systemctl} reboot --firmware-setup, mode "default"'';
            # back to normal: Enter or Escape
            "Return" = ''mode "default"'';
            "Escape" = ''mode "default"'';
          };
          "${modeResize}" = {
            "j" = "resize shrink width 10 px";
            "k" = "resize grow height 10 px";
            "i" = "resize shrink height 10 px";
            "l" = "resize grow width 10 px ";
            "Left" = "resize shrink width 10 px";
            "Down" = "resize grow height 10 px ";
            "Up" = "resize shrink height 10 px ";
            "Right" = "resize grow width 10 px ";

            # back to normal: Enter or Escape or $mod+r
            "Return" = ''mode "default"'';
            "Escape" = ''mode "default"'';
            "${mod}+r" = ''mode "default"'';
          };
        };
      };
    };
  };
}
