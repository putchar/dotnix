{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customHomeManagerModules;
in {
  config = lib.mkIf cfg.sway.enable {
    ## we will need to override it someday or make a new pr in nixpkgs
    ## https://github.com/NixOS/nixpkgs/blob/nixos-unstable/pkgs/tools/graphics/wdisplays/default.nix#L19
    ## https://github.com/luispabon/wdisplays
    ## the new repository is here https://github.com/artizirk/wdisplays

    home.packages = [
      pkgs.wdisplays
    ];

    services.kanshi = {
      enable = false;
      profiles = {
        oscaro-undocked = {
          outputs = [
            {
              criteria = "eDP-1";
              position = "0,0";
              mode = "1920x1080@60.002Hz";
            }
          ];
        };
        oscaro-docked = {
          outputs = [
            {
              criteria = "eDP-1";
              position = "0,0";
              mode = "1920x1080@60Hz";
            }
            {
              criteria = "DP-3";
              position = "1920,0";
              mode = "1920x1080@60Hz";
            }
            {
              criteria = "DP-4";
              position = "3840, 0";
              mode = "1920x1080@60Hz";
            }
          ];
        };
      };
    };
  };
}
