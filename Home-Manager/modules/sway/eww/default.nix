{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customHomeManagerModules;
in {
  config = lib.mkIf cfg.sway.enable {
    ## https://nix-community.github.io/home-manager/options.html#opt-programs.eww.enable
    programs.eww = {
      enable = true;
      package = pkgs.eww-wayland;
      configDir = ./.;
    };
  };
}
