{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customHomeManagerModules.sway;
in {
  imports = [
    ./kanshiConfig.nix
    ./makoConfig.nix
    ./rofiConfig.nix
    ./swayConfig.nix
    ./thunar.nix
    ./waybarConfig.nix
    ./waybarStyle.nix
    ./wofiConfig.nix
    ./yambarConfig.nix
  ];

  ## https://arewewaylandyet.com/
  ## https://shibumi.dev/posts/my-way-to-wayland/
  ## https://github.com/swaywm/sway/wiki/Useful-add-ons-for-sway
  options.customHomeManagerModules.sway = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable sway config globally or not
      '';
    };
  };
}
