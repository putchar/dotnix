{
  config,
  pkgs,
  lib,
  inputs,
  ...
}: {
  # nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager
  # nix-channel --update
  # nix-shell '<home-manager>' -A install

  imports = [
    ## we import directories
    ## and nix will look for a `default.nix` file inside
    ./i3
    ./sway
    ./vscode
    ./wallpaper

    ## we import flat `*.nix` file
    ./alacrittyConfig.nix
    ./bashConfig.nix
    ./fontConfig.nix
    ./gitConfig.nix
    ./gtkConfig.nix
    ./kubeConfig.nix
    ./sshConfig.nix
    ./starshipConfig.nix
    ./vimConfig.nix
    ./bluetoothConfig.nix
  ];

  ## https://nix-community.github.io/home-manager/options.html#opt-systemd.user.startServices
  systemd.user.startServices = "legacy";

  # Do not show news on generation rebuild.
  news.display = "silent";
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
  ## https://nix-community.github.io/home-manager/options.html#opt-programs.firefox.enable
  ## TODO https://github.com/NixOS/nixpkgs/issues/157061#issuecomment-1035168081
  ## https://nixos.wiki/wiki/Accelerated_Video_Playback
  programs.firefox = {
    enable = true;
    package = pkgs.wrapFirefox pkgs.firefox-unwrapped {
      extraPolicies = {
        ExtensionSettings = {};
      };
    };
    profiles = {
      putchar = {
        id = 0;
        path = "zbyiwsqk.default";
        isDefault = true;
        ## those options are available with the `about:config` option in the url bar
        settings = {
          "browser.startup.homepage" = "about:profiles";
          "media.rdd-ffmpeg.enabled" = true;
          "media.ffmpeg.vaapi.enabled" = true;
          "media.navigator.mediadatadecoder_vpx_enabled" = true;
          "media.ffvpx.enabled" = false;
          "media.rdd-vpx.enabled" = false;
          "media.rdd-vpx.enabledPreformatted" = "text";
          "media.rdd-process.enabled" = false;
          "gfx.webrender.all" = true;
        };
        #bookmarks = {
        #  wikipedia = {
        #    keyword = "wiki";
        #    url = "https://en.wikipedia.org/wiki/Special:Search?search=%s&go=Go";
        #  };
        #  "kernel.org" = {
        #    url = "https://www.kernel.org";
        #  };
        #};
      };
    };
  };

  ## cf https://github.com/nix-community/home-manager/issues/2942#issuecomment-1119760100
  #nixpkgs.config.allowUnfree = true;
  nixpkgs.config.allowUnfreePredicate = pkg: true;

  xdg.userDirs = {
    enable = true;
    createDirectories = true;
  };

  home.packages = [
    pkgs.nix-tree
    #pkgs.nix-du
    pkgs.xdg-utils
    pkgs.gnupg
    pkgs.colmena
    pkgs.tree
    (
      pkgs.google-chrome.override
      {
        commandLineArgs = [
          "--ignore-gpu-blocklist"
          "--enable-gpu-rasterization"
          "--enable-zero-copy"
          #
          ##"--force-dark-mode"
          ##"--enable-features=WebUIDarkMode"
          #
          "--enable-features=VaapiVideoEncoder,VaapiVideoDecoder,CanvasOopRasterization"
          ##"--use-gl=desktop"
          #"--enable-oop-rasterization"
          #"--enable-raw-draw"
          #
          #"--use-vulkan"
          ##"--enable-vulkan"
          ##"--enable-features=Vulkan"
          #"--disable-reading-from-canvas"
          #"--disable-sync-preferences"
          #"--enable-features=UseOzonePlatform"
          #"--ozone-platform=wayland"
          #"--ozone-platform-hint=auto"
          #"--gtk-version=4"
          #"--start-maximized"
        ];
      }
    )
    #pkgs.firefox

    # (pkgs.spotify.override {deviceScaleFactor = 2;})
    pkgs.spotify
    pkgs.bottom
    pkgs.gotop
    pkgs.iotop
    pkgs.gnome.gnome-disk-utility
    pkgs.kleopatra
    pkgs.baobab
    pkgs.signal-desktop
    pkgs.slack

    pkgs.gopass
    pkgs.yt-dlp
    (pkgs.vlc.override {
      waylandSupport = true;
      withQt5 = true;
    })

    ## gcloud kube
    pkgs.google-cloud-sdk

    pkgs.libsecret
    pkgs.graphviz
    pkgs.cdrkit
    pkgs.ffmpegthumbnailer
    pkgs.keepassxc
    pkgs.xournalpp

    ## cf https://github.com/rust-unofficial/awesome-rust#applications
    pkgs.ripgrep
    pkgs.broot
    pkgs.duf
    pkgs.procs
    pkgs.du-dust
    pkgs.httpie
    pkgs.curlie
    pkgs.inetutils
    pkgs.gping
    pkgs.fd
    pkgs.lsd
    pkgs.tealdeer
    pkgs.cheat
    pkgs.dogdns
    #pkgs.lapce
    (
      pkgs.writeShellScriptBin "wan_ip.sh" ''
        ${pkgs.curl}/bin/curl https://ifconfig.me
      ''
    )
    (
      pkgs.writeShellScriptBin "listDevices.sh" ''
        ${pkgs.util-linux}/bin/lsblk -o name,mountpoint,label,size,uuid
      ''
    )
    pkgs.skypeforlinux
  ];

  home.file."current-system-packages".text = let
    packages = builtins.map (p: "${p.name}") config.home.packages;
    sortedUnique = builtins.sort builtins.lessThan (lib.unique packages);
    formatted = builtins.concatStringsSep "\n" sortedUnique;
  in
    formatted;

  programs.gpg.enable = true;
  programs.obs-studio = {
    enable = true;
    plugins = [pkgs.obs-studio-plugins.wlrobs];
  };
  home.sessionVariables.GTK_THEME = "Numix";

  services.gpg-agent = {
    enable = true;
    enableBashIntegration = true;
    pinentryPackage = pkgs.pinentry-gnome3;

    #pinentryFlavor = "gnome3";
  };
  programs.direnv = {
    enable = true;
    enableBashIntegration = true;
    nix-direnv.enable = true;
  };
}
