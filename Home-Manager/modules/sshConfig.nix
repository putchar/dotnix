{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customHomeManagerModules.sshConfig;
in {
  options.customHomeManagerModules.sshConfig = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable sshConfig globally or not
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    programs.ssh = {
      enable = true;
      compression = true;
      serverAliveInterval = 10;
      serverAliveCountMax = 2;
      matchBlocks = {
        ## my own stuff
        "*.putchar.net 192.168.122.*" = {
          user = "putchar";
          identityFile = "~/.ssh/id_rsa";
          forwardAgent = true;
        };

        "192.168.1.* rpi*" = {
          user = "pi";
          identityFile = "~/.ssh/id_rsa";
        };

        "desktop framework hetzner-1 appenin" = {
          user = "putchar";
          identityFile = "~/.ssh/id_rsa";
          forwardAgent = true;
        };

        "51.15.237.157" = {
          user = "bastion";
          identityFile = "~/.ssh/id_ed25519_scaleway";
          forwardAgent = true;
          port = 61000;
        };

        "10.42.3.* 10.42.4.* 10.42.5.* tf-*" = {
          user = "ubuntu";
          identityFile = "~/.ssh/id_ed25519_scaleway";
          forwardAgent = true;
          proxyJump = "bastion@51.15.237.157:61000";
        };

        "api" = {
          hostname = "10.42.5.10";
          user = "ubuntu";
          identityFile = "~/.ssh/id_ed25519_scaleway";
          forwardAgent = true;
          proxyJump = "bastion@51.15.237.157:61000";
        };

        "10.*" = {
          user = "nixos";
          identityFile = "~/.ssh/id_ed25519_s3ns";
          forwardAgent = false;
        };
      };
    };
  };
}
