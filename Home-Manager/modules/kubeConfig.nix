{
  config,
  pkgs,
  ...
}: {
  home.packages = [
    ## cf https://kubernetes.io/docs/tasks/tools/included/optional-kubectl-configs-bash-linux/
    ## cf https://github.com/NixOS/nixpkgs/blob/master/pkgs/build-support/setup-hooks/install-shell-files.sh#L52
    ## cf https://nixos.org/manual/nixpkgs/stable/#fun-substituteInPlace
    ## cf https://nixos.org/manual/nixpkgs/stable/#sec-pkg-overrideAttrs
    ## cf https://github.com/NixOS/nixpkgs/blob/nixos-unstable/pkgs/applications/networking/cluster/kubernetes/kubectl.nix
    #(pkgs.kubectl.overrideAttrs (oldAttrs: rec {
    #  installPhase = ''
    #    runHook preInstall
    #    install -D _output/local/go/bin/kubectl -t $out/bin
    #    installManPage docs/man/man1/kubectl*
    #    installShellCompletion --cmd kubectl \
    #      --bash <($out/bin/kubectl completion bash) \
    #      --zsh <($out/bin/kubectl completion zsh)
    #
    #    ln -s $out/bin/kubectl $out/bin/k
    #    installShellCompletion --cmd k --bash --name k.bash <($out/bin/kubectl completion bash)
    #    substituteInPlace $out/share/bash-completion/completions/k.bash --replace "complete -o default -F __start_kubectl kubectl" "complete -o default -F __start_kubectl k"
    #
    #    runHook postInstall
    #  '';
    #}))
    pkgs.kubectl
    (pkgs.stdenvNoCC.mkDerivation {
      name = "kubernetes-alias";

      ## https://github.com/NixOS/nixpkgs/issues/23099#issuecomment-964024407
      dontUnpack = true;

      nativeBuildInputs = [pkgs.installShellFiles];

      installPhase = ''
        mkdir -p $out/bin
        cp ${pkgs.kubectl}/bin/kubectl $out/bin/k

        installShellCompletion --cmd k --bash --name k.bash <($out/bin/k completion bash)
        substituteInPlace $out/share/bash-completion/completions/k.bash --replace "complete -o default -F __start_kubectl kubectl" "complete -o default -F __start_kubectl k"
      '';
    })

    pkgs.kubectx
    pkgs.kubeswitch
    pkgs.k9s
  ];
}
