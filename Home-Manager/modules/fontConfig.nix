{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customHomeManagerModules.fontConfig;
in {
  options.customHomeManagerModules.fontConfig = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable font config globally or not
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    # cf https://nix-community.github.io/home-manager/options.html#opt-fonts.fontconfig.enable
    # cf https://github.com/nix-community/home-manager/blob/master/modules/misc/fontconfig.nix#blob-path
    # cf https://nixos.wiki/wiki/Fonts
    fonts.fontconfig.enable = true;

    home.file.".local/share/fonts/clacon2.ttf".source = pkgs.fetchurl {
      url = "https://webdraft.eu/fonts/classic-console/fonts/clacon2.ttf";
      sha256 = "sha256-4QsY2bvZyhZ72Y4601UeVd5v7XAVvTqCHQFRYkXyQTw=";
    };

    home.packages = [
      pkgs.nerdfonts
      pkgs.font-awesome
      #(
      #  pkgs.stdenv.mkDerivation {
      #    pname = "classic-console-neue";
      #    version = "master";
      #
      #    src = pkgs.fetchurl {
      #      name = "classic-console-neue";
      #      url = "https://webdraft.eu/fonts/classic-console/fonts/clacon2.ttf";
      #      sha256 = "sha256-4QsY2bvZyhZ72Y4601UeVd5v7XAVvTqCHQFRYkXyQTw=";
      #    };
      #
      #    dontConfigure = true;
      #    dontUnpack = true;
      #    installPhase = ''
      #      mkdir -p "$out/share/fonts/truetype"
      #      cp * "$out/share/fonts/truetype/"
      #    '';
      #  }
      #)
      #(
      #  pkgs.stdenvNoCC.mkDerivation rec {
      #    fetchurl {
      #      name = "classic-console-neue";
      #
      #      url = "https://webdraft.eu/fonts/classic-console/fonts/clacon2.ttf";
      #
      #      postFetch = ''
      #        mkdir -p $out/share/fonts
      #        cp clacon2.ttf $out/share/fonts/
      #      '';
      #
      #      sha256 = "027cf62zj27q7l3d4sqzdfgz423lzysihdg8cvmkk6z910a1v368";
      #
      #      #meta = {
      #      #  description = "Monospaced font family for user interface and coding environments";
      #      #  maintainers = with lib.maintainers; [relrod];
      #      #  platforms = with lib.platforms; all;
      #      #  homepage = "https://adobe-fonts.github.io/source-code-pro/";
      #      #  license = lib.licenses.ofl;
      #      #};
      #    }
      #  }
      #)
    ];

    #home.packages = [
    #  (pkgs.nerdfonts.override {
    #    fonts = [
    #      "Hack"
    #      "DejaVuSansMono"
    #      "JetBrainsMono"
    #      "FiraCode"
    #      "FiraMono"
    #      "Ubuntu"
    #      "UbuntuMono"
    #      "CascadiaCode"
    #      "BitstreamVeraSansMono"
    #    ];
    #  })
    #  pkgs.font-awesome
    #];
  };
}
