{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customHomeManagerModules.gitConfig;
in {
  options.customHomeManagerModules.gitConfig = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable gitConfig globally or not
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    home.packages = [
      pkgs.tig
      pkgs.git-extras
      pkgs.git-crypt
      pkgs.ghorg
      pkgs.glab
      pkgs.gh
      ## https://difftastic.wilfred.me.uk/git.html
      pkgs.difftastic
    ];

    programs.git = {
      package = pkgs.gitFull;
      enable = true;
      lfs.enable = true;
      #      delta.enable = true;
      difftastic.enable = true;
      ignores = ["*.vscode" "*.direnv"];
      userName = "Slim Cadoux";
      userEmail = "slim.cadoux@gmail.com";
      aliases = {
        lg = "log --graph --pretty=tformat:'%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%an %ai)%Creset'";
        #lg = "!git log --pretty=format:\"%C(magenta)%h%Creset -%C(red)%d%Creset %s %C(dim green)(%cr) [%an]\" --abbrev-commit -30";
        d = "diff";
        s = "status";
        sw = "switch";
        swcr = "switch -C";
        del = "branch -D";
        br = "branch --format='%(HEAD) %(color:yellow)%(refname:short)%(color:reset) - %(contents:subject) %(color:green)(%(committerdate:relative)) [%(authorname)]' --sort=-committerdate";
        save = "!git add -A && git commit -m 'chore: commit save point'";
        undo = "reset HEAD~1 --mixed";
        res = "!git reset --hard";
        done = "!git push origin HEAD";
        lazy = ''!f() { git add -A && git commit -m "$@" && git push; }; f'';
        pushmr = ''
          !f() { git pull && git checkout -b "$@" && git add -A && git commit -m "$@" && git push origin "$@"; }; f'';
        purge = ''
          !git branch --merged | egrep -v "(^\*|master|main|dev|stage)" | xargs git branch -d'';
      };
      includes = [
        {
          condition = "gitdir:/home/putchar/Documents/gitlab.com/appenin/";
          contents = {
            user = {
              email = "slim.cadoux@appenin.fr";
              name = "Slim Cadoux";
            };
            core = {
              sshCommand = "ssh -i /home/putchar/.ssh/id_ed25519_appenin";
            };
          };
        }
        {
          condition = "gitdir:/home/putchar/Documents/gitlab.com/putchar/";
          contents = {
            user = {
              email = "slim.cadoux@gmail.com";
              name = "Slim Cadoux";
            };
            core = {
              sshCommand = "ssh -i /home/putchar/.ssh/id_rsa";
            };
          };
        }
        ## cf https://nix-community.github.io/home-manager/options.html#opt-programs.git.includes._.contents
        {
          condition = "gitdir:/home/putchar/Documents/gitea/mog/";
          contents = {
            user = {
              email = "slim.cadoux@s3ns.io";
              name = "Slim Cadoux";
              #signingKey = "AC5C590F22665724";
              signingKey = "slim.cadoux@s3ns.io";
            };
            commit = {
              gpgSign = true;
            };
            core = {
              sshCommand = "ssh -i /home/putchar/.ssh/id_ed25519_s3ns";
            };
          };
        }
      ];

      ## https://nix-community.github.io/home-manager/options.html#opt-programs.git.extraConfig
      extraConfig = {
        init = {
          defaultBranch = "main";
        };
        core = {
          editor = "vim";
          excludesFile = "";
        };
        #        pager = {
        #          diff = "${pkgs.delta}/bin/delta";
        #          log = "${pkgs.delta}/bin/delta";
        #          reflog = "${pkgs.delta}/bin/delta";
        #          show = "${pkgs.delta}/bin/delta";
        #        };
        #        delta = {
        #          plus-style = "syntax #012800";
        #          minus-style = "syntax #340001";
        #          #syntax-theme = "Monokai Extended";
        #          navigate = true;
        #          side-by-side = true;
        #        };
        remote = {
          prune = true;
        };
      };
    };
  };
}
