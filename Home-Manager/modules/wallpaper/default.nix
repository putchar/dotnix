{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.customHomeManagerModules.wallpaper;
in {
  options.customHomeManagerModules.wallpaper = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        whether to enable wallpaper globally or not
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    home.packages = [pkgs.nitrogen];

    systemd.user.services."nitrogen-restore" = {
      Unit = {
        Description = "restore the wallpaper";
        After = ["graphical-session-pre.target"];
        PartOf = ["graphical-session.target"];
      };
      Install.WantedBy = ["graphical-session.target"];
      #serviceConfig.Restart = "always";
      #serviceConfig.RestartSec = 2;
      Service.ExecStart = "${pkgs.nitrogen}/bin/nitrogen --restore";
    };
  };
  #  home.file.".config/xfce4/helpers.rc".source = ./helpers.rc;
}
