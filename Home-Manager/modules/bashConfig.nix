{
  config,
  pkgs,
  ...
}: {
  programs.bat = {
    enable = true;
    ## cf https://github.com/sharkdp/bat#customization
    config = {
      map-syntax = ["*.jenkinsfile:Groovy" "*.props:Java Properties"];
      ## to list available themes `bat --list-themes`
      theme = "ansi";
    };
  };

  ## https://nix-community.github.io/home-manager/options.html#opt-programs.fzf.enable
  programs.fzf = {
    enable = true;
    enableBashIntegration = true;
  };

  programs.mcfly = {
    enable = true;
    enableBashIntegration = true;
    fzf.enable = true;
    fuzzySearchFactor = 3;
  };

  programs.bash = {
    enable = true;
    bashrcExtra = ''
      export MCFLY_RESULTS=50

      export VAGRANT_DEFAULT_PROVIDER="libvirt";
      export HISTTIMEFORMAT="%F %T ";
      export EDITOR="vim";
      case ''${TERM} in
        xterm*|rxvt*|Eterm|alacritty*|aterm|kterm|gnome*)
          PROMPT_COMMAND=''${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033]0;%s@%s:%s\007" "''${USER}" "''${HOSTNAME%%.*}" "''${PWD/#$HOME/\~}"'
        ;;
        screen*)
          PROMPT_COMMAND=''${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033_%s@%s:%s\033\\" "''${USER}" "''${HOSTNAME%%.*}" "''${PWD/#$HOME/\~}"'
        ;;
      esac

      tmssh () {
        ${pkgs.openssh}/bin/ssh -t "$@" "sleep 1 && tmux attach-session -t scadoux_tmux || tmux new-session -s scadoux_tmux";
      }


      get_executable_path () {
        ${pkgs.lsd}/bin/lsd -la $(${pkgs.which}/bin/which $1)
      }
    '';

    shellAliases = {
      ".." = "cd ..";
      cat = "${pkgs.bat}/bin/bat -p";

      l = "${pkgs.lsd}/bin/lsd -lah";
      la = "${pkgs.lsd}/bin/lsd -la";
      ll = "${pkgs.lsd}/bin/lsd -l";
      ls = "${pkgs.lsd}/bin/lsd";

      find = "${pkgs.fd}/bin/fd";
      grep = "${pkgs.ripgrep}/bin/rg";
      dig = "${pkgs.dogdns}/bin/dog";
      g = "${pkgs.git}/bin/git";
      gp = "${pkgs.git}/bin/git pull";
      gs = "${pkgs.git}/bin/git status";
      gd = "${pkgs.git}/bin/git diff";
      clean_branches = "${pkgs.git}/bin/git for-each-ref --format '%(refname:short)' refs/heads | ${pkgs.gnugrep}/bin/grep -v 'master\\|main' | ${pkgs.findutils}/bin/xargs ${pkgs.git}/bin/git branch -D";
      lazygit = "g lazy";
      gpushmr = "g pushmr";
      screenshot = "flameshot gui";
      appenin = "cd ~/Documents/gitlab.com/appenin";
      immo-minute = "cd ~/Documents/gitlab.com/immo-minute/";
      github = "cd ~/Documents/github.com";
      gitlab = "cd ~/Documents/gitlab.com";
      dotnix = "cd ~/Documents/gitlab.com/putchar/dotnix";
      nixpkgs = "cd ~/Documents/github.com/NixOS/nixpkgs";
      lockscreen_enable = "${pkgs.systemd}/bin/systemctl --user start swayidle.service";
      lockscreen_disable = "${pkgs.systemd}/bin/systemctl --user stop swayidle.service";
      gaming_on = ''
        ${pkgs.systemd}/bin/systemctl --user stop swayidle.service && ${pkgs.sway}/bin/swaymsg 'output HDMI-A-1 disable' && ${pkgs.libvirt}/bin/virsh start win11-vfio
      '';
      proxy_sockv5 = "${pkgs.openssh}/bin/ssh -D 1337 -q -C -N rpi00";
      dallow = "${pkgs.direnv}/bin/direnv allow";
      ddeny = "${pkgs.direnv}/bin/direnv deny";
      ## readlink -f $(which python)
      gep = "get_executable_path";
    };
  };
  home.packages = [
    pkgs.shellcheck
  ];
}
