{
  config,
  pkgs,
  lib,
  ...
}: {
  customHomeManagerModules = {
    fontConfig.enable = true;
    gitConfig.enable = true;
    gtkConfig.enable = true;
    sway = {
      enable = true;
      #extraUserSettings = {
      #};
    };
    sshConfig.enable = true;
    starship.enable = true;
    vim.enable = true;
    vscode = {
      enable = true;
      extraUserSettings = {
        "window.zoomLevel" = 2;
      };
    };
  };
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "putchar";
  home.homeDirectory = "/home/putchar";
  home.packages = [
    pkgs.gnome.gnome-boxes
    pkgs.mpv
  ];

  ## also check (spotify.override {deviceScaleFactor = 2;})

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  # home.stateVersion = "21.05";
}
