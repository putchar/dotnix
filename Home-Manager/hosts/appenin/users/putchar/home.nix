{
  config,
  pkgs,
  ...
}: {
  # nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager
  # nix-channel --update
  # nix-shell '<home-manager>' -A install

  customHomeManagerModules = {
    fontConfig.enable = true;
    gitConfig.enable = true;
    gtkConfig.enable = true;
    sway.enable = true;
    sshConfig.enable = true;
    starship.enable = true;
    vim.enable = true;
    vscode.enable = true;
    bluetooth.enable = true;
  };
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "putchar";
  home.homeDirectory = "/home/putchar";

  programs.ssh.includes = [
    "/home/putchar/Documents/gitlab.com/putchar/dotnix/Home-Manager/hosts/appenin/users/putchar/secrets/sshConfig"
  ];

  systemd.user.tmpfiles.rules = [
    "d /home/putchar/.ssh/sockets 0755 putchar users"
  ];
  #systemd.user.sessionVariables.SSH_AUTH_SOCK = "/run/user/1001/keyring/ssh";

  ## does now work in pure mode
  #  home.file.".aws/credentials".source = /home/putchar/Documents/gitlab.com/putchar/dotnix/Home-Manager/hosts/appenin/users/putchar/secrets/aws_credentials;

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  # home.stateVersion = "21.05";
}
