{
  config,
  pkgs,
  ...
}: {
  # nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager
  # nix-channel --update
  # nix-shell '<home-manager>' -A install

  customHomeManagerModules = {
    fontConfig.enable = true;
    gitConfig.enable = true;
    gtkConfig.enable = true;
    sway.enable = true;
    sshConfig.enable = true;
    starship.enable = true;
    vim.enable = true;
    vscode.enable = true;
  };
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "putchar";
  home.homeDirectory = "/home/putchar";
  home.packages = [
    pkgs.telegram-desktop
  ];

  programs.helix.enable = true;

  programs.ssh.includes = [
    "/home/putchar/Documents/gitlab.com/putchar/dotnix/Home-Manager/hosts/appenin/users/putchar/secrets/sshConfig"
  ];

  systemd.user.tmpfiles.rules = [
    "d /home/putchar/.ssh/sockets 0755 putchar users"
  ];

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  # home.stateVersion = "21.05";
}
